# 19 March 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Daniel Chivvis, Allie Williams, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #34 | Draft LSTN handbook | Updates below&mdash;will be trying to add remote activities this spring | NC |
| #36 | Research radio reception laws in Moldova | No update | PP |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #40 |  | XX |
-->

## Updates

### LSTN Updates

* Safety concerns
  * Nico and Daina drafted a [letter](https://docs.google.com/document/d/1FbOM1rhu7GgR3D145ScSsuYIXfNqkiJ6zBc77WqTtLE/edit?usp=sharing) for partner libraries to address potentional safety issues
* Sent out emails to our partner libraries, since timeline is shifting due to COVID-19 issues
  * Ground station build at CPL is not going forward, and we don't know when we can reschedule; this means we can't try out any of our educational activities yet
  * Remote activities?
    * Once SatNOGS DB is updated, we can try making remote activities using it
    * We're still hoping to get activities out to partner libraries this spring
  * We still want ground station installations to go forward by the fall! Current goal is to get kits to our partners before the winter
* Got quotes on translating letters and other materials into Spanish
  * No dialects yet; since one of our partner libraries is in Chile, should we try to get a translation in Chilean Spanish?
    * Best bet is to ask librarians at Biblioteca de Santiago for advice
* Fredy might want to talk with Nico about best settings for Wolbach ground station (gain, etc.) to get good signals

### MetaSat Updates

* Researching space segment
  * Trying a different strategy: Researching each system, and then making JSON-LD file for each
  * Planning on 9 systems right now, so JSON-LD will be written in 9 chunks
  * Using a book, *Low Earth Orbit Satellite Design*, for help and crosschecking ideas with commercial sector
  * Shooting for the end of March for the first draft of the JSON-LD
    * URIs will probably come in April
    * Our [timeline](https://gitlab.com/metasat/meetings/-/blob/master/2020-02-06_meetingMinutes.md#updated-2020-timeline) is still mostly up-to-date, space segment pushed back a week
  * Keep checking Riot, will be posting for feedback
* New MetaSat website is up! [schema.space](https://schema.space/)
  * Term definitions are on a Google spreadsheet [here](https://docs.google.com/spreadsheets/d/1es-QUAeJ_WVmCnhAfT7SHAg-5_W-pfCY3p2-zhXpG6A/edit#gid=0) if you have feedback

### LSF Updates

* What is LSF's timeline for the pilot?
  * Client update is done, bugs have been fixed
  * Now, making database changes, updating models and fields, for example

<!--
### Wolbach Updates

* x
-->
