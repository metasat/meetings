# 30 September 2021 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Katie Frey, Nikoletta Triantafyllopoulou, Allie Tatarian  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; LISA - Library and Information Services in Astronomy)*  
<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  

### LSTN Updates

* Cambridge Public Library update
  * Gave a lot of good feedback on the LSTN handbook (mostly pictures)
  * Got through their network issues, but still need to set up the ground station
    * Will get us notes on how the ground station works with their network
  * Will be finishing up ground station installation soon

<!--### MetaSat Updates  

* x

### LSF Updates

* x-->

### Wolbach Updates

* Didn't get the NASA funding
  * One critique: How would we grow the LSTN network? Can Nikoletta or Pierros help contribute using knowledge of how the LSF network has grown?
  * Two more: Not clearly tied tightly enough to NASA, evaluation plan was mostly qualitative, not quantitative
* Potentially break up future funding, and work on future outreach strategies
* Who are people we can talk to and work with now, before we get funding? Potentially people who can help us even come up with a proposal and get funding.
