# 3 June 2021 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daniel Chivvis, Fredy Damkalis, Katie Frey, Nikoletta Triantafyllopoulou, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* [LSTN social media plan](https://docs.google.com/document/d/1iEfkMSns7qVpInyDT-zesjlJr4UC8O1M0hlfZvrzI50/edit?pli=1) was shared with us
    * We want more libraries to join for the future; might want to create a channel on something like Element to keep the libraries engaged; right now it is difficult to get in touch with the partner libraries
    * Is there a way we can get testimonials for the future? That will help us showcase them on social media
        * Both of these are good things to talk to Nico about the next time he's here
    * GitLab communications board; if you have any ideas, or things to inform audience about, open an issue on communications board; link will be shared on MetaSat channel

### MetaSat Updates 

* A week or two away for submitting MetaSat paper for review
* Plan for the summer:
    * Will be going through existing issues on GitLab
    * Paper: Example JSON-LD file has extra concepts, so will be updating the vocab with those
    * Creating WikiData items for any spacecraft with an identifier, populated with MetaSat concepts
        * Reached out to in-the-sky.org database to see if we can get lists from them; will also rope in others, like Jonathan McDowell

<!--### LSF Updates

* x

### Wolbach Updates

* x-->
