# 30 April 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #41 |  | XX |
-->

## Updates

### LSTN Updates

* Starting to ship out LSTN kits - Marathon, TX was shipped, the rest are waiting for details from FedEx.
* The LSTN handbook is on [Zenodo](https://doi.org/10.5281/zenodo.3776517)!
  * Also working on handbook page on [LSTN website](https://lstn.wolba.ch/handbook/)
* Educational displays?
  * A [Twitter account](https://twitter.com/matplotlib/status/1232892565952679937?s=20) sharing SatNOGS displays made using a Python program; might reach out to see if we can use the same tools to make educational materials
  * Another idea - [SatNOGS monitor](https://wiki.satnogs.org/SatNOGS_Monitor)
    * Like a terminal representation - see an example [here](https://community.libre.space/t/satnogs-station-monitor/2802)

### MetaSat Updates

* For SatNOGS, What is the best way to hold "observation" or "observation number" for users, ground stations, and satellites?
  * For example, on [this page](https://network.satnogs.org/stations/1378/), see the "Observations" term on the left; this holds the total number of observations, past and present
  * Right now in this [SatNOGS terms file](https://gitlab.com/metasat/metasat-schema/-/blob/master/Terms/SatNOGS_terms.jsonld#L99) it looks like this:
  
``` JSON
"observation": {
    "id": [array of URIs],
    "numberOf": "current number",
    "futureNumber": "future number"
}
```
* Is there a better way to convey this idea?
  * maybe "count" and "cumulativeCount" or "futureCount"
  * [Quantity](https://www.wikidata.org/wiki/Property:P1114) on Wikidata
    * Then what terms for future count?
    * Also [number of entities](https://www.wikidata.org/wiki/Q614112) on Wikidata, but it's an item not a property
    * Will make an issue and post this to Riot for feedback
* Space segment research is wrapping up, working on organization
    * Made an [organizational chart](https://drive.google.com/open?id=1pgzvMCSTJc9jrbwOhiTkY-IiJjMWWv8B) of space segment concepts
  * Tomorrow, will mint URIs for space segment terms
    * Will be able to find them at [the MetaSat website](https://schema.space/metasat/)
  * Next week, will redo/reorganize ground segment (especially URI updates)
* Electrical power systems JSON-LD [terms file](https://gitlab.com/metasat/metasat-schema/-/blob/master/Terms/ElectricalPowerSystems.jsonld) is complete
* SatNOGS [terms file](https://gitlab.com/metasat/metasat-schema/-/blob/master/Terms/SatNOGS_terms.jsonld) is also ready to look at
  * [Organization for SatNOGS terms](https://drive.google.com/open?id=18F-YNSaXKCCU-R9dqBZSvq2OU2K9mPeh)
  * SDR terms in SatNOGS DB - Not super clear, which are important?
  
<!--
### LSF Updates

* x

### Wolbach Updates

* x
-->
