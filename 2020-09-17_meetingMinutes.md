# 17 September 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daniel Chivvis, Fredy Damkalis, Katie Frey, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  
<!--
### LSTN Updates

* x
-->

### MetaSat Updates

* [Website](https://schema.space) is fully up-to-date with elements and crosswalks
* Next steps: RDF files and official release (v1.0!)
  * Improving documentation - on GitLab and website
  * Restructuring GitLab
* Student assistant at library is working on a MetaSat JSON-LD generator
  * VERY early stages right now&mdash;working on proof-of-concept

### LSF Updates

* Started testing stations for new waterfall artifact
  * Next step: Deploying production clients (optional at first)
* Satellite ID: Deploying changes in production to move forward
  * Should at least be starting in development by the end of next week

<!--
### Wolbach Updates

* x
-->