# 21 May 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #41 |  | XX |
-->

## Updates

### LSTN Updates

* Got an email from our Moldovan partner library in Chişinău - the country is in the process of reopening, so we are hoping to start working with the library on LSTN soon.

### MetaSat Updates

* Fredy has started looking through the terms but has more to review, will make comments soon
* Made progress sorting elements into element families, to help organize them on the website. Hoping to finish up today; this is not live on the website yet.
* Started crosswalking terms
  * Meeting with student workers to get help with crosswalk
* Working on documentation to go along with website.

### LSF Updates

* Still working on artifacts, started with the very simple, will be moving forward soon.
  * Have started with waterfalls, but are moving to waterfall-as-data (using data to create the image, which leads to other functionalities, like filtering)
  * This artifact is the first one they are experimenting with, and is a little more complex than current waterfall
  * Looking for ways to store all the data
  * Implemented in DB, also have some experimental clients that will be providing artifacts

### Wolbach Updates

* Daina update: Talked with Josh Greenburg (from Sloan Foundation) about potential next-phase ideas
  * Next step is to talk about best short- vs long-term goals, because financial impacts of COVID-19 for the rest of the year are currently unknown
