## August 8, 2019 MetaSat Meeting Minutes

**Time:** 10am EST / 17:00 (5pm) EEST  
**Attendees:** Pierros Papadeas, Nico Carver, Daniel Chivvis, Allie Williams  
**Meeting purpose**: Updates from both teams

*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; ERD - Entity Relationship Diagram)*

<!-- #### Action Review  
Future meetings will start with review of GitLab issues/former action items: What progress was made, what was completed, what can we do to complete stalled issues? -->

#### Logistics  
<!-- Wolbach needs to consider getting an external mic or headphone splitter to make it easier to chat without echo -->
* Issues with echo during meetings
    * LSF send Wolbach info about speaker for meetings
    * Wolbach will look into getting a headphone splitter

#### Action Items  
* [LSTN kit document update][LSTN]
    * Raspberry Pi 4
    * Amplifiers required
    * Filtering optional depending on region
* LSF: [figure out information they can send Wolbach info about data that goes to their dashboard specifically][ERD]
    * Dataflow, ERDs, datasets, etc. and anything else that might be helpful
* LSF: [send Wolbach information about licensing in LSTN shortlist countries][licensing] (esp. Namibia and Zambia)
* LSF: [look into licensing in Botswana one last time][Botswana]
    * With contact in South Africa
* LSF: [put together list of islands off of Namibia for Wolbach][islands]
* [LSTN shortlist][shortlist]
    * Wolbach: email libraries and other points of contact in the LSTN shortlist (Brazil, Fiji, Namibia, Zambia)
    * Wolbach: double-check libraries in Fiji


#### LSTN Updates  
**LSF feedback on candidate countries:**   
* *Brazil*
    * LSF has a strong community built here already
    * Already SatNogs station in southern Brazil (São Paulo), so might look into northern Brazil  
* *Fiji*
    * Wolbach may have eliminated Fiji bc it doesn't have a strong public library network
      * Main library is a Carnegie library
    * From LSF perspective, Fiji is a strong candidate
      * Remote islands can be good for radio/satellite communication
* *Botswana*
    * Not clear how to get radio clearance, but LSF has a South African contact they can talk to
    * Botswana also has slow Internet and power/electric issues  
* *Namibia*
    * LSF thinks this is the strongest African candidate
        * Organized local radio programs
        * Also a "gateway" to some nearby islands  
* *Zambia*
    * LSF's second choice in Africa
      * not as strong of a radio presence as Namibia
      * landlocked
    * Has a stronger public library presence than Namibia  
* *India*
    * Depends on the region; different areas of India have very different infrastructure

**New short list**: Brazil, Fiji, Namibia, Zambia.  
Will pick 2, one Southern and one Eastern.

**Tribal libraries**: Nico put together document of what we're trying to do with public libraries.  
Order: Cambridge first, then a tribal library, then international (slow rollout)  

**LSTN kit update**:   
Migrating kits to Raspberry Pi 4 is high priority for LSF  
Amplifier is necessary, not optional  
Filtering is optional based on location  

#### MetaSat Updates   
* Currently working through fields, elements and relationships  
* Right now, thinking of how MetaSat can be used to support dashboards (use case)
    * How would LSF benefit from better organization of data used for their dashboard?
    * Does LSF have example of dataflow from high to low level, existing ERDs or datasets that would help with this use case?

<!-- Links -->
[ERD]: https://gitlab.com/metasat/meetings/issues/1
[LSTN]: https://gitlab.com/metasat/meetings/issues/6
[licensing]: https://gitlab.com/metasat/meetings/issues/7
[Botswana]: https://gitlab.com/metasat/meetings/issues/8
[islands]: https://gitlab.com/metasat/meetings/issues/9
[shortlist]: https://gitlab.com/metasat/meetings/issues/10
