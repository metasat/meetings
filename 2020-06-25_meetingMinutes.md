# 25 June 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |
| #41 | Add MetaSat primer to schema repository | No update | AW |
| #42 | Make plans for updating website | Expect update next week | AW |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates

### LSTN Updates

* Packages still moving with FedEx
  * Moldovan package in Chișinău, but it's being held in customs. We're working to resolve the issue.
  * Package to Santiago, Chile is still in US but on its way.

### MetaSat Updates

* Crosswalk
  * Narrowing down list of crosswalk sources to make sure we focus on the most relevent
  * By next week should be on website!
* Sorting tool
  * Want back on site soon, but former version is very outdated, so have to update
  * Should be up by next week
* MetaSat paper
  * Still working, might ask for feedback in a few weeks
* What do we do if terms become deprecated?
  * Add versioning to URI? Redirects?
  * Do not want to delete any URIs as we add new versions. Will probably add a "deprecated" tag, and look into how to add a "superseded by" tag.

### LSF Updates

* MetaSat implementation is still moving forward. Working on public UI now, so anyone should be able to export. Hoping to have demo by start of next week.
* Also working on client to support uploading HDF5 artifacts.

<!--
### Wolbach Updates

* x
-->
