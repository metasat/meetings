# 5 March 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Pierros Papadeas, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #34 | Draft LSTN handbook | Ongoing | NC |
| #36 | Research radio reception laws in Moldova | No update despite repeated attempts (radio clubs and national telecomms association); next step is to call | PP |

**All other actions were completed or are no longer relevant.**

<!--
### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #40 |  | XX |
-->

## Updates

### LSTN Updates

* Nico set up Wolbach ground station
  * [See settings for Lime SDR here](http://lstn.wolba.ch/img/basic-config.png)
    * Pierros shared his setup on the Metasat [Riot channel](https://riot.im/app/#/room/#metasat:matrix.org)
    * Some of these settings will be in the "Advanced" section; see [setup instructions](https://wiki.satnogs.org/Software_Defined_Radio#Instructions_to_activate_the_bias-t_for_SatNogs_Observations_automatically:) on the SatNOGS wiki
    * Latest client release was yesterday, if you update now will be on the latest release
  * Will try to get ground station up and running today


### MetaSat Updates

* Progress!
  * Daniel at [Access 2 Space](https://civspace.jhuapl.edu/News-and-Events/events/Access2Space/) conference last week
    * Very helpful since we are working on space and launch segments now
    * More metadata for launch vehicle (LV) stuff, like interfaces to connect payload and LV
    * Definitely want to include "Lessons learned" in Mission segment
  * Worked on [QUBIK example JSON-LD file](https://gitlab.com/metasat/metasat-schema/-/blob/master/Examples/QUBIK_EXAMPLE.jsonld)
    * Thanks Vasilis for flagging syntax errors!
    * This is just a start based off document received from Pierros 
    * Definitely want to include modulation type, decoders, mission designators
  * Expect space segment to expand rapidly over the next few weeks
    * Only comms part is close to "finished," because it's similar to ground segment comms stuff
    * Should we add tuning ranges? Vasilis said no, it's a hardware thing, will you have to move to a different channel if it's congested? Will tell others that you have the capability
* MetaSat Licensing ([issue 5 on schema repo](https://gitlab.com/metasat/metasat-schema/issues/5)) 
  * We want same license as schema.org 
    * Creative Commons Attribution-ShareAlike 4.0 International License ([CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/))
    * Could there be issues if we don't use [CC0](https://creativecommons.org/publicdomain/zero/1.0/)? For example, if we incorporated MetaSat into WikiData 
      * We don't think so, see [Wikilegal here](https://meta.wikimedia.org/wiki/Wikilegal/Database_Rights), but looking for a second opinion
      * Want to keep on top of this though; will mostly have to worry about US law
      * Can maybe get pro bono advice from Harvard cyberlaw students
  * Might also consider a [W3C patent](https://www.w3.org/Consortium/Patent-Policy-20040205/) 
    * Looks like we would need this to be incorporated into schema.org
    * All we know right now is that we'd have to prove that we're royalty free
    * LSF might have contacts to talk to about this
* Website progress
  * [New website](http://test.schema.space/) is going well
  * Want to launch by next Friday (13 March)
    * Goal is to have ground segment URIs minted by then
    * Adding [303 redirects](https://www.w3.org/TR/cooluris/#choosing) for RDF-XML and JSON-LD files
      * Might add plain JSON, too
      * Discussion about URIs on [Issue 27](https://gitlab.com/metasat/metasat-schema/issues/27) in the MetaSat repo

### LSF Updates

* New Raspberry Pi mods released!
  * Still finding a few errors with the Lime SDR; please report if you see it (check Wiki [troubleshooting page](https://wiki.satnogs.org/Troubleshooting)) 
  * The error might have to do with the exact Lime SDR, so even if our ground station doesn't get the error, our partners might
    * We bought all of the Lime SDRs together so hopefully will be consistent


### Wolbach Updates

* Cambridge Science Festival
  * All set for April 25th for build; Cambridge Explores the Universe at CfA is on 26th.
  * Hopefully: Fredy and Manthos come on 23rd, talk and build on the 25th, and possibly at CEtU too? 
    * Depends on flight dates, will figure out the details soon
    * CEtU: We will probably have a table for MetaSat, since the CfA is hosting we can do whatever types of activities we want
    * LSF might have a good activity (tracking satellites live w NOAA)
      * Small grounds can quickly build a small antenna (may or may not involve soldering), take an SDR and phone or laptop; then, they can go outside and track NOAA satellites right there!
      * Can hear and see signal live, watch images form line-by-line, very interactive
      * Could do this at Cambridge Science Festival too? Will bring this up at meetin
        * it does have a time commitment from participants, it can be a 2-6 hour activity (depending on how long build takes and when NOAA satellites are above)
      * Might want to consider folding this into LSTN handbook for other libraries, too; an activity focusing on older audiences, like high schoolers
