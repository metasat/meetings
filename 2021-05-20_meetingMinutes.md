# 20 May 2021 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* Blog post has been presented to the LSF core team.
  * Feedback from Elkos: How can people add a new language?
  * On any of the component pages (such as [this one](https://hosted.weblate.org/projects/library-space-technology-network/lstn-handbook-chapter-1/)), there is a "Start new translation" button on the bottom. You must be logged in to a Weblate account to use it. If your language or dialect isn't there, can make a request through Weblate and it will email Allie.
* Blog post should be published on Monday
* Progress with Chile: Installed ZeroTier to make it easier to troubleshoot directly from afar.
  * They are still having issues with their LimeSDR, it will not keep a stable connection. Will replace soon with a different SDR.

<!--### MetaSat Updates 

* x
-->
### LSF Updates

* Have an endpoint update coming soon, but need to work through some merge request. Once those are done, can put it on the dev instance.
<!--
### Wolbach Updates

* x-->
