# 29 October 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Pierros Papadeas, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  

### LSTN Updates

* Waiting for feedback about LSTN educational materials.
* Finishing up LSTN proposal&mdash;LSF signed off!
  * Has not been submitted yet

### MetaSat Updates

* Daniel is still working on the MetaSat paper
* New relationships with other similar projects
  * [PMpedia](https://pmpedia.space/) (UC Boulder)&mdash;similar to SPOON
  * [N2YO](https://www.n2yo.com/database/) is another database we want to talk to&mdash;Daniel will be scheduling a meeting soon
* MetaSat website and GitLab changes are happening
  * If there is any text that should be updated, let Allie know
  * Adding crosswalk text to website
* Student workers were going through existing crosswalks, looking for missing terms and the like
  * They're done now! So what can they do next?
  * Might start adding MetaSat terms to Wikidata
    * MetaSat Wikidata property ID needed first

### LSF Updates

* Some progress with Satellite ID
    * Still [discussions](https://community.libre.space/t/satnogs-satellite-id/6781) about actual format


<!--
### Wolbach Updates

* x
-->