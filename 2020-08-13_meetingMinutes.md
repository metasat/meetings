# 13 August 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Nico Carver, Daniel Chivvis, Katie Frey, Fredy Damkalis, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates

### LSTN Updates

* Partner libraries in Marathon and Moldova have been doing test builds!
* Contact with people outside of project; some people have asked about SatNOGS/Libre Space projects that are out of LSTN's scope
  * Is there a good contact at Libre Space for questions like this?
* Next week we're talking more in depth about LSTN educational materials

### MetaSat Updates

* Updating element families, going through elements one-by-one for editing
  * Most definitions/descriptions come from Wikipedia/Wikidata, so, descriptions are pretty general/vague
    * Making them more spacecraft specific, e.g. spacecraft subsystem, not just subsystem
* Reorganized MetaSat google drive
* Working on RDF files for each element
* Working on a new JSON-LD example file (capturing PICARD, a French mission)
  * Will have elements for Space, Ground, and Launch segments
  * Incorporating Jonathan McDowell's data, and meeting with him soon
  * Also updating existing examples
* Met with Robbie Robertson (Small Spacecraft Reliability Institute); will be attending their biweekly meetings; they're adopting Metasat!
* Met with David Palmer from Los Alamos; Leading ELROI, a project to assign IDs to each space object with a laser component that's on payload
  * Figuring out best way to capture its metadata (might require ~10 elements); if they make a database for this, may use MetaSat
* Adding new crosswalks
* Adding MetaSat to the [Open Metadata Registry](http://metadataregistry.org/) over the next few months
* Working on updates to the website; making more mobile friendly (beginning stages), and polishing up accessibility stuff


### LSF Updates

* UI changes to [SatNOGS DB](https://db.satnogs.org/) are live
* Working on final tests for waterfall artifacts

<!--
### Wolbach Updates

* x
-->
