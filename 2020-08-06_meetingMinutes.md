# 6 August 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Katie Frey, Fredy Damkalis, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**

<!--
### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates

### LSTN Updates

* Scheduled date to talk about educational ideas later this month (August 20th)
  * Going to continue to develop Morse Code activity in the meantime
  * [Education brainstorming document](https://docs.google.com/document/d/1o7Cz3Y7vJt5PXMqKKHVPNv_UU0rPmzWBu7ADVpfSu0E/edit?usp=sharing)
    * Forwarded doc to Cambridge Public Library, too
* Setting up email list for LSTN libraries
  * Will help libraries communicate with each other

### MetaSat Updates

* The [MetaSat website](https://schema.space/) is updated!
  * Element edits, new families
  * Still needs some polishing
  * Addition of [resources page](https://schema.space/resources)
    * Includes both stuff we wrote and external links
* Need to update example files
  * Also might do a new example, too; high level file to describe a generic mission
* Doing research into how MetaSat will/can be used
  * Might build a small database using MetaSat
    * Probably using a graph format, and [Terminus DB](https://en.wikipedia.org/wiki/TerminusDB)
    * If this is useful, might create a resource using this example
    * Also might help describe use cases in our paper!
  * Also - will revisit RDF eventually
  * Chat with Fredy about SatNOGS API?
* Attending SmallSat virtual meetings
  * Finding potential MetaSat users!

### LSF Updates

* Continuing to work on new UI for DB; soon should be ready (tomorrow or weekend will move to production side)
  * Will announce on Twitter, Riot, etc when it's ready
  * Working on first waterfall artifact to go on DB

### Wolbach Updates

* We got in touch with [Jonathan McDowell](http://www.planet4589.org/jcm/index.html), he wants to update his DB to make it easier to inject into SatNOGS
  * He's willing to give us all of the metadata that he records
    * Will want to make sure that we can cover it all with MetaSat; we think this will be fine right now
      * Might use his data as a use case instead of ISS