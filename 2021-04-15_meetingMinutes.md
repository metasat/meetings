# 15 April 2021 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, <!--Daniel Chivvis,--> Fredy Damkalis, Katie Frey, <!--Eleftherios Kosmas, Pierros Papadeas,--> Nikoletta Triantafyllopoulou, <!--Vasilis Tsiligiannis,--> Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* Having station dropout issues with RTL-SDR right now
  * This is an old bug, strange to see it crop up again.
  * Want to keep fiddling with parts, need to figure that out soon so we can get it budgeted
* Working with Cambridge Public Library, they have feedback for the LSTN handbook
  * Might do a revision in early summer
  * Start with putting together a list of known issues; could we make these into GitLab issues? We have a [LSTN group](https://gitlab.com/library-space-technology-network) now
  * If we change SDR, will need new pictures, too
* Website update
  * [Translations page](https://lstn.wolba.ch/translations/)
  * [Weblate tutorial](https://lstn.wolba.ch/weblate/)
    * Probably state at the top of the page something about how interfaces change, and to let us know if you get stuck
      * Weblate's interface has changed since the screenshots on this page were taken...
    * Put a "last updated [date]" at the bottom
  * Librarian at Biblioteca de Santiago had issues using Weblate without an account
    * Add "set up an account" as a task on the page
    * Look into issues with suggesting without account, this should be possible but may need some clarity
  * Allie will talk with Katie later this month to brainstorm about how to help users with navigation
* Blog post update
  * No update yet, have been busy with urgent communication issues
  * Plan to start working on the blog on Tuesday

### MetaSat Updates 

* Next week one of the Wolbach student workers will join us; she is working on a MetaSat JSON-LD generator and will demo next week

### LSF Updates

* Had to change a model to fix a bug, but this made code very complicated; made another big change to simplify things

### Wolbach Updates

* Have an outline for ROSES proposal, submitting the notice of intent to apply today
