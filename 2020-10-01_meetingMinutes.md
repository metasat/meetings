# 1 October 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; ARDC - Amateur Radio Digital Communications)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |
<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  

### LSTN Updates

* How can we start expanding more?
  * Library-specific STEM initiatives, NASA at your library
* More feedback: Put on SatNOGS community forum?
* Working on [ARDC](https://www.ampr.org/) grant to continue/expand

### MetaSat Updates

* RDF file for MetaSat terms - putting it on the side now
  * Instead using JSON-LD files, similar to Codemeta, and making terms and crosswalks available as CSV files
* Updating website today
  * Adding some "missing" elements that are important for initial release of MetaSat; should be live by the end of the day today
* Improving GitLab repo - documentation and organization
* Working more with Becca (Wolbach student worker) on JSON-LD generator

<!--
### LSF Updates

* Continuing development on client and DB; nothing special to report (chugging along)

### Wolbach Updates

* x
-->