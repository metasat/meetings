# 1 April 2021 MetaSat Meeting Minutes

**Time:** 15:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, 
Eleftherios Kosmas, <!--Pierros Papadeas,--> Nikoletta Triantafyllopoulou, <!--Vasilis Tsiligiannis,--> Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* [Blog post outline](https://docs.google.com/document/d/1jnuq0anR2IEux_uJxNfI4i-IPP2etoG3W2ZCbyEyS9A/edit?pli=1) has been shared
  * Nikoletta will probably start to write in about a week and a half
  * Want the post to mention that we are trying to lessen the barrier of language
  * Also we are *starting* with the handbook but will probably use Weblate to translate other materials as well
* We have made a quick [webpage about translations](https://lstn.wolba.ch/translations/) on the LSTN website
  * Next, we will be adding labeled screenshots of the Weblate interface and a few short walkthroughs of simple concepts
  * Maybe in the farther future, might make some video walkthroughs
* Crossposting blog and website
  * Nikoletta looking into posting on Reddit. Quora didn't seem promising
  * How would we describe the audience we want to reach?
    * Fluent in languages, as well as familiarity with the technology (might not be entirely required)
    * Current Weblate users might already have the technical know-how
  * Elkos has been in contact with ESA space education office&mdash;might want to contact them, they will have access to people who are fluent and target educators
    * Could be especially helpful for translating educational materials? However they will want to focus on national educational standards and priorities
* Nico and Pierros met recently to talk about improvements to the LSTN kit
  * Having issues with the software-defined radio (SDR) (dropout, noisy) (Lime SDR mini); will try RTL-SDR instead
  * More educational ideas - might cost more money; contact L-band and geostationary satellites that can pull down cool Earth pictures
    * Can find some example pictures on [this website](http://usradioguy.com/)
  * Maybe natural radio astronomy (hydrogen band is in L-band, for example)

### MetaSat Updates 

* Met with ARDC (Australian Research Data Commons), which hosts the [UAT](https://astrothesaurus.org/) for us
  * Hoping they will host MetaSat, and give us access to tools to make MetaSat real RDF linked data and create an API
  * They gave us a [demo](https://demo.vocabs.ardc.edu.au/viewById/483) of Pool Party (set of tools to make this happen)
  * Might be complicated because the tools support hierarchy,and MetaSat isn't actually hierarchical, but uses groupings (families and segments)
    * Unclear exactly how we will represent this using the tools provided
  * Should find out if we can work with ARDC on this soon...usually restrict access to projects based in Australia
* MetaSat JSON-LD generator is coming along well; seeing a new demo next week so expect updates then

### LSF Updates

* Not quite done with development
  * Have some big changes coming up

<!--
### Wolbach Updates

* x-->
