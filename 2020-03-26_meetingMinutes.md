# 26 March 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Pierros Papadeas, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Vasilis Tsiligiannis, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #34 | Draft LSTN handbook | No update | NC |
| #36 | Research radio reception laws in Moldova | No update | PP |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x
-->

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #40 | Feedback on spreadsheets for MetaSat terms (especially batteries) | LSF |

## Updates

### LSTN Updates

* Trying to work with collaborators but they're all closed right now
  * Unclear if librarians are working form home or not
* Getting translations of letters and other materials to South American Spanish
  * [Example letter in Spanish](https://docs.google.com/document/d/1GD0y0_zbMqU9Xjvea38OJJJuOO2rC2HiXYgRHpZ0l2c/edit#)
* [Wolbach ground station](https://network.satnogs.org/stations/1378/) was malfunctioning
  * Raspberry Pi was not talking to Lime SDR
  * This might be the Lime SDR issue LSF was afraid of
  * Pierros sharing some potential fixes&mdash;will need to make sure that these get to the partner libraries, too (we want everyone to be on the same version, easier to troublesheet)
    * This is an update of the LimeSDR, so we'll just update them before we sent them
    * Also current SatNOGS client setup has the fix already

### MetaSat Updates

* Have been moving forward on the space segment: Attitude control and electrical power systems
  * Next: Comms then propulsion, then maybe on-board computer
  * Daniel has been working hard on battery metadata
    * [Spreadsheet with battery terms](https://docs.google.com/spreadsheets/d/1MtqC5iZnX6mb7udkXo9hkvEGw-8x10vLqZYFbSNjfnY/edit?usp=sharing)
      * LSF is sharing this with people who have the expertise
  * Looking through [SPOON](https://spoonsite.com/openstorefront/login/index.html;jsessionid=C300C6B8EF134D06C3463024C33A1763#/) (**S**mallSat **P**arts **O**n **O**rbit **N**ow) and [SatSearch](https://satsearch.co/) database to see what metadata they store.
  * Two places where need help: 
    * Can we have someone who is familiar with the subsystems look over for accuracy? Mostly batteries
    * Getting help from other student workers at Wolbach to find synonyms and examples for terms
      * They'll be added to Riot chat: Becca, Shea and Sam
* Derivative values&mdash;should we keep them?
  * Yes! We don't know what terms and ideas different groups are starting with
  * We are not creating a schema or a standard, we are creating terms and a skeleton that people can use and build upon
* How do we differentiate between terms in different places?
  * For example: battery cells and battery packs both have energy density, but there is only one term for energy density.
  * Depends on where it is in the structure. Elements will be repeated but exist in different places
  * Terms are meant to be general enough to be used in different contexts
  * Getting too granular will make MetaSat harder to use and harder to upkeep
  * So in this example, instead of making "batteryCellEnergyDensity" and "batteryPackEnergyDensity," we will have one "energyDensity" term that can nest under *either* "batteryCell" or "batteryPack"
* The terms in the [battery spreadsheet](https://docs.google.com/spreadsheets/d/1MtqC5iZnX6mb7udkXo9hkvEGw-8x10vLqZYFbSNjfnY/edit?usp=sharing) might be too detailed for satellite purposes&mdash;why aren't other pieces this granular?
  * We can use feedback to help figure out which parts can be pared down!
  * A lot of these terms were taken from manufacturing metadata for car batteries. They may not be relevant for non-manufacturers or people buying batteries to be used in spacecrafts
  * We're also missing some space-specific stuff, such as radiation shielding

#### New timeline

* Daniel will be staying on through October, so we have more time to build the schema itself
* URIs for space segment should be minted by the end of April
* When does LSF need the terms?
  * Can implement ground segment and space segment separately
  * What about mission and orbital elements?
    * More or less done, can fast-track URIs for these

<!--
### LSF Updates

* x

### Wolbach Updates

* x
-->
