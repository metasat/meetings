# 4 February 2021 MetaSat Meeting Minutes

**Time:** 15:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Vasilis Tsiligiannis, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |

## Updates  


### LSTN Updates

* x
-->
### MetaSat Versioning 

* [Code of Conduct draft](https://docs.google.com/document/d/16qQ-fTwofHQynn5Cdbwf-S-6nyIbBKKjZGu1C9VFtM0/edit)
  * Should we say anything in here about, for example, recommending escalation if necessary? 
  * Maybe instead adding something like "Our Responsibilities," and state that we will ask for permission before recording or using real names, for example
  * Have to be careful with names: real names may be necessary for copyright reasons with certain contributions.
* [Contributing guidelines](https://docs.google.com/document/d/1XmUUmjYTEwKw6EG-h72b7VZqp9eg9Z7PP_hBDpeSJWU/edit)
  * [SatNOGS contributing guidelines](https://gitlab.com/librespacefoundation/satnogs/satnogs-db/blob/master/CONTRIBUTING.md#sign-your-work-the-developers-certificate-of-origin), "Signing your work" section
    * Does this apply to a project like MetaSat? What is the best way to balance these legal/copyright issues with personal privacy? 
    * Might include something like "if you want to make a pull request, must sign your real name."
    * Could add to a new section on "How to make a pull request"
  * Will probably create templates for submitting issues (or even multiple templates for multiple "types" of issues)
* Should Code of Conduct and Contributing Guidelines be two different files or combined into one? 
  * Probably make two different files, maybe link to each other.
* [Release file about page](https://docs.google.com/document/d/1lxFIdAR-VD4dOuUdqzORAulvh58LFtBql9g8TKdre-c/edit)
* Idea for contributors: create "Subject area specialist roles" to help foster contributions and curate. These would be more formal, specified roles that people can "sign up" for.
  * This will allow them to submit new concepts, as well as editing existing ones
  * What should the roles be? Should contributors come up with their own roles?
  * Might try to make 5-10 specific roles, plus allow users to submit new ones
  * [Wikipedia page](https://en.wikipedia.org/wiki/Spacecraft_design) Daniel used for inspiration on potential subject areas to focus on
* Probably will make GitLab issues for each of these documents, as well as the specialist roles and issue templates
  * This will be a good place to make comments/discussions
  * Will share with LSF team when they're up, and hopefully can discuss more next week.

<!--
### LSF Updates

* x

### Wolbach Updates

* x
-->