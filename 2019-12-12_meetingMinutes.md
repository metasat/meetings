# December 12, 2019 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Vasilis Tsiligiannis   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; BOM - Bill of Materials [for LSTN kit]; HDF5 - Hierarchical Data Format 5, a file format used to both store and organize data)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #10 | Contact potential partner libraries | Moldova and Namibia are moving forward, taking different strategy with rural US | NC |
| #22 | Email specific individuals for feedback | Keep open until AAS.  | DC |
| #34 | Draft LSTN handbook | No updates. | NC |
| #35 | Make decisions on LSTN laptop | No updates. | DB, NC |
| [MetaSat Schema 4](https://gitlab.com/metasat/metasat-schema/issues/4) | LSF: give feedback to Daniel on new schema | Daniel would like feedback before AAS meeting, January 4, 2020 | -- |

**All other actions were completed or are no longer relevant.**

<!--
#### Logistics  
* 


## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #36 | ---------------- | --- | -->

## Updates

### LSTN Updates

* Speaking with Namibian Ministry of Education, Arts and Culture about proposed ground station
* Also still in talks with contact in Moldova--they have been sent a proposal  for LSTN project
* Rural US--looking at other libraries.
  * Now, looking at American Southwest, near Mexican border.
  * Hoping for partner by the end of January 2020
* Will order the parts for all 5 LSTN kits.
  * Hardware question: BOM lists "Power over Ethernet injector." 110-240 input voltage--should work for US and Europe, but the cord itself will be different in different places.

### MetaSat Updates

* We want to expand our schema to work with artifacts--important for SatNOGS DB
  * In this context, an artifact is some piece of information sent from a satellite to a ground station
  * Want to make syste client-focused: how do we share info with them?
  * Experimenting with HDF5 file format--see more in LSF Updates section.
  * Might have a separate meeting just to discuss the artifacts. Radio vs optical data (right now it's all radio but want to expand)
* Another MetaSat goal: Tooling--making it easier for people to make JSON-LD files. 
  * Want to use this to describe what is in the HDF5 file, for example.
* Allie: Currently writing up a piece for the MetaSat Schema README
  * Will describe the four JSON-LD algorithms--Expansion, compaction, flattening and framing--with examples.
  * Should be up later today (US Eastern time) or early next week.
* Send questions about JSON-LD to Allie, and Wolbach send questions about HDF5 to LSF.


### LSF Updates

* Right now SatNOGS client only catches audio, radio, and waterfalls. Want to update it so it can grab other information and add it to SatNOGS DB (like images, etc)
  * Will have to update APIs for this, too.
* HDF5 file format--acts as a container for many times of data. It can take observations, waterfalls, etc. and bundle them into one file. The data can be transferred more easily in that way
  * We also want the HDF5 files to include adds context information.
  * Currently experimenting packing waterfall data into HDF5. Then will move into delimited data.
  * What metadata can we add about the observation? This should come from MetaSat! 
  * Eventually the data will move into the SatNOGS DB
  * Will work with an API as well
* Are there good ways to describe HDF5 objects yet?
  * Probably not. May have to add this to the schema
  * What metadata should be external, internal, both?

### Wolbach Updates

* Planning for AAS meeting in early January
  * We would like an up-to-date schema to get feedback.
  * Can we get feedback from LSF first? About elements and descriptions, as well as the organization of elements.
  * Daniel wants an additional, more simple method of feedback. (not the sorting tool or the actual LD--more like a spreadsheet)
