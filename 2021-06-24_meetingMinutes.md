# 24 June 2021 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* Working with librarian in Chile still to fix their ground station. Next step will be to take the ground station apart and put it back together.
* Nico is going to the Wolbach library tomorrow to adjust the ground station to hopefully reduce noise.
* Working with Cambridge Public Library on next steps; they are reopening soon.

### MetaSat Updates 

* Daniel met with Robbie Robertson (SSRI knowledge base) to talk about ideas for working with MetaSat
    * Working on creating a glossary for the knowledge base, and to use MetaSat terms for their API
    * Will work with other S3VI technologies (like SPOON), as well 

<!--### LSF Updates

* x
-->
### Wolbach Updates

* NASA Space Act Agreement has finally been drafted! They've asked for feedback from Wolbach
