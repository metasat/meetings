# 11 February 2021 MetaSat Meeting Minutes

**Time:** 15:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Katie Frey, Vasilis Tsiligiannis, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* Nico meeting with Chilean library this afternoon
* Reconnected with Cambridge Public Library, on track to make ground station and put it up this spring
* Met with Science Education department
  * Will help us put together proposals
* Also met with [STAR Net libraries](http://www.starnetlibraries.org/) (space-focused education in libraries)
  * (they fund [NASA @ My Library](www.starnetlibraries.org/about/our-projects/nasa-at-my-library/))
  * Want to collaborate writing a proposal for NASA [ROSES](https://science.nasa.gov/researchers/sara/grant-solicitations/roses-2020/release-research-opportunities-space-and-earth-science-roses-2020); call for proposals should come out next week
  * They are interested in both educational materials and including SatNOGS
  * US-based, but what if we had US libraries "partner" with international libraries? (sister relationship)
* Trying to figure out setting up Weblate with LSTN
  * Will be making a GitLab repository with files for LSTN handbook, but have some questions about how to make it connect with Weblate
  * Nico is making flat XML files using InDesign, and trying to figure out how to import the same once translations are done

### MetaSat Updates 

* Wikidata: Making guidelines for making new Wikidata concepts from MetaSat
  * Right now, around 500 MetaSat terms are not in Wikidata
* Daniel putting together folder with necessary next steps and the like; guides and such for side projects
  * Can be used by student workers or anyone else in the future who will be working on MetaSat
  * Will eventually make a bunch of issues on GitLab for these projects and changes for future releases
  * Any suggestion for things, let Daniel know and he will add it

<!--
### LSF Updates

* x

### Wolbach Updates

* x
-->