# 8 July 2021 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Nico Carver, Daniel Chivvis, Katie Frey, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* Wolbach station is still having issues connecting to radio, even though we replaced the radio
    * So, the issue may not be the LimeSDR, might be another issue with the Wolbach station
        * Marathon, TX station still has LimeSDR and hasn't had drop-out issues
    * Will try replacing USB cable, and ask for advice on Riot channel
    * We got Amazon USB cables...the quality might not be great. As we standardize the kit, we should figure out a good quality USB cables to send with everyone
        * Anker brand might be good to look into
        * Might be nice to test out several different brands to see what's best.

### MetaSat Updates 

* Daniel has been reviewing pages for SSRI knowledge base. Being released to the public on the 14th.

<!--### LSF Updates

* x

### Wolbach Updates

* x-->
