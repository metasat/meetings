## July 25, 2019 MetaSat Meeting Minutes

**Time:** 10am EST / 17:00 (5pm) EEST  
**Attendees:** Daina Bouquin, Pierros Papadeas, Nico Carver, Daniel Chivvis, Allie Williams  
**Meeting purpose**: Updates from each group; giving Wolbach people the chance to ask Pierros questions

*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; ERD - Entity Relationship Diagram; SAA - Space Act Agreement)*

**Logistics**  
* Make email list
* Set up GitLab
    * Meeting between Allie and Pierros about GitLab
      * Allie and Pierros will touch base later via email
* Make a new wiki for MetaSat/LSTN?
* LSF: Update reference kit information on [Wiki][wiki-link]
* [SAA][nasa-saa] research
    * How many do we need? (1-3)
* Regular meeting time between LSF and Wolbach
    * Thursdays at 10a EST/5p EEST?
* Make sure everyone has a Riot account

**Action items**  
* Pierros: [Share existing ERD for SatNogs DB, any other relevant documents][ERD]
* Pierros: [Connect Wolbach with core members of community/LSF team who have already been discussing changing their schema][community]
* LSTN tasks
    * Wolbach: Share docs about candidates with LSF
    * LSF: [Send feedback on candidates][intl]
    * LSF: [Input on rural candidates; any potential logistical issues with Woksape Tipi?][rural]
    * LSF: [Give feedback on Nico's LSTN doc as it gets updated with information about the kit][kit]  
* Wolbach: Drafting use cases <!-- Right now Wolbach is keeping their to-dos on Asana, hence no links/issues. May change this for accountability/consistency's sake -->
    * SatNogs specific
    * General use
* Science engagement ideas
    * Long-term goal
    * Create curriculum/ideas to give partner libraries
    * Work with Harvard Science Ed department?

**Reference kit questions**  
* Concerning the ground station kit for libraries: A specific radio and computer mentioned on Wiki. Does Pierros have one particular thing in mind for the kit at different libraries, or will it depend?
    * Specific components recommended because of both price and quality
    * For some items, LSF hasn't been able to recommend specifics because there are procurement issues (can't find a consistent provider); it can be hard to find something that works for everyone.
    * Check [links on the Wiki][build], recs for reference kit may change soon
      * More expensive but higher quality setup
    * Since this is a *reference* setup, people/libraries can change things if they would like
* S-band for kit. Does that make it less fun? (Can't talk to ISS, weather sats)  
    * Might change to L-band (clearer signals). Will give them options, might have rotators, different bands, etc.

**LSTN update: candidate countries, etc.**  
* **Southern/Eastern hemisphere**
    * Botswana
    * Namibia
    * Zambia
    * Concerns for African countries
      * How to setup radio frequencies/get the permissions?
      * Fees, legal considerations?
      * These countries do have local radio clubs
* **Southern hemisphere**
    * Brazil
      * LSF has ground stations here and have expanded recently
* **Eastern hemisphere**
    * India
      * LSF has ground stations and a community here
* **Rural US**
    * Tribal Libraries on Native American reservations in MT, MN, SD
    * All away from major city centers
    * Woksape Tipi in SD is top choice
        * Community College/public library
        * Serves Sioux tribe
        * 90% of county is NA
* ***For all libraries:*** Want to give them as much info on ground station as possible, without being overwhelming or too jargony

**LSF update**  
Working on administrative/maintenance stuff so they can be ready to hit the ground running with Science Library project ASAP

[wiki-link]: https://wiki.satnogs.org/Main_Page
[build]: https://wiki.satnogs.org/Build
[nasa-saa]: https://en.wikipedia.org/wiki/Space_Act_Agreement
[kit]: https://gitlab.com/metasat/meetings/issues/5
[rural]: https://gitlab.com/metasat/meetings/issues/4
[intl]: https://gitlab.com/metasat/meetings/issues/3
[community]: https://gitlab.com/metasat/meetings/issues/2
[ERD]: https://gitlab.com/metasat/meetings/issues/1
