# 05 August 2021 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daniel Chivvis, Katie Frey, Nikoletta Triantafyllopoulou, Allie Williams  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*  
<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  

### LSTN Updates

* Published last social media post last week
  * Preparing a report/overview of how the campaign went, should have it ready next week

### MetaSat Updates  

* Working with Tod Robbins (LSF) mapping SatNOGS with wikidata stuff? Using MetaSat
* Reorganizing GitLab, going through old issues and making new ones
* We might be getting access to [PoolParty](https://www.poolparty.biz/) from [ARDC](https://ardc.edu.au/) soon!

<!--### LSF Updates

* x

### Wolbach Updates

* x-->
