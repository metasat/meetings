# 15 July 2021 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Pierros Papadeas, Nikoletta Triantafyllopoulou, Allie Williams  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*  
<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**
-->

## Logistics  

* Allie won't be here next week, Nico will take minutes instead

<!--
## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  

### LSTN Updates

* Nico working on version 2 of LSTN handbook; should be done around end of month; then will work on adding translations
  * Version 2 is mostly new pictures
* LSTN stations use two versions of SDR: RTL-SDR and Lime SDR; different stations seem to work better with one or the other. This means that issues with stations might be caused by something else, like cord issues
* Santiago, Chile is going to take their station down and reconnect everything; they are having low signal issues
  * Is it overwhelmed by local transmissions? Check the scale on the waterfall; does it compare to the same satellites on other ground stations?
    * Assuming the stations are similar. Maybe Chile vs Wolbach or Marathon?
  * Lots of things potentially interact with signals
* LSTN email address! contact@lstn.wolba.ch

### MetaSat Updates  

* Met with SSRI (Small Spacecraft Reliability Institute); they're using MetaSat for their glossary and API; Daniel will help with tailoring MetaSat terms for their knowledge base
  * Will make it easier to integrate with stuff including SatNOGS

<!--### LSF Updates

* x

### Wolbach Updates

* x-->
