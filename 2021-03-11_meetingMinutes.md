# 11 March 2021 MetaSat Meeting Minutes

**Time:** 15:00 UTC  
**Attendees:** Daina Bouquin, Daniel Chivvis, Katie Frey, 
Eleftherios Kosmas, Nikoletta Triantafyllopoulou, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |

## Updates  
-->

## LSTN Outreach

* Communication officer Nikoletta is here, helping to promote LSTN translations project
  * Coordinates communication of Libre Space Foundation; social media posts and the like
* What is our goal?
  * Want to reach out to a new audience for our translation project (want Spanish and Romanian translations right now)
  * Need more community building right now; attracting attention and getting feedback
* Plan: Will use channels through both LSF and SatNOGS to promote the translations
    * Want to write a blog post for the LSF blog; will start by publishing that and putting it in a newsletter
* Places to reach out to: International library groups? Anyone else LSF knows that can boost the signal?
    * Nikoletta will look into this; look for groups on other channels; maybe Quora or Reddit?
* Is Weblate straightforward for translators? Should we have an explainer somewhere?
    * LSF community already has some people who have done translations (for Libre Space manifesto)
    * Because of this, this audience may not need like a tutorial
    * We can start by reaching out to them, and then get feedback and see what people are getting stuck on, then maybe make a clarifying document
        * Could post in LSF contributors sub-forum
    * After that, then do a real "Call to action" for the wider public
* Start making a page for the LSTN site with a Weblate tutorial, for people who want more information on how to translate
  * Katie will set Allie up with a login for the LSTN page
* Actions for next week: Nikoletta will give us a step-by-step analytical plan and an outline for the blog post; will also look for different groups to talk to next

<!--
### MetaSat Updates 

* x

### LSF Updates

* x

### Wolbach Updates

* x
-->