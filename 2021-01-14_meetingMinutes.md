# 14 January 2021 MetaSat Meeting Minutes

**Time:** 15:00 UTC  
**Attendees:** Daina Bouquin, Daniel Chivvis, Fredy Damkalis, Katie Frey, Pierros Papadeas, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; ARDC - Amateur Radio Digital Communications)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  

### LSTN Updates

* LSTN translations: Figuring out a platform to use for crowdsourced translations, then prioritizing languages to pursue for translations 
  * Want to get translations BEFORE we reach out, so that different places will know that they will have easily-accessible materials already
  * Want to translate the LSTN handbook for now, and the rest of our stuff eventually, as well

### MetaSat Updates

* [MetaSat v1.0.0](https://gitlab.com/metasat/metasat-toolkit/-/releases) is released!
  * [Archived on Zenodo](https://doi.org/10.5281/zenodo.4437493)

### LSF Updates

* Crunch time for grants and such for Satellite ID
* Meeting with UNOOSA (United Nations Office for Outer Space Affairs) soon about Satellite ID 
  * An open standard for satellite identification, answering an ESA call for ideas
  * Pushing to make sure that whatever standard is used is OPEN and accessible
  * Want to work with [Access to Space for All initiative](https://www.unoosa.org/oosa/en/ourwork/access2space4all/index.html) (helping non-spacefaring countries do space missions) 
  * Want to work with them to get SatNOGS kits to developing nations...maybe tied in with LSTN? Hopefully Wolbach people can be part of this
* Technical update: Changes on database API, helps to import data from other sources that use MetaSat

### Wolbach Updates

* ARDC grant response: No 
  * Will hopefully get a more concrete followup within the next few weeks

* Space Act Agreement has been approved! Waiting for some signatures
  * Small Spacecraft Systems Virtual Institute (S3VI) has approved two SAAs, one with Harvard College Observatory and one with Smithsonian Astrophysical Observatory
  * Can get NASA support for MetaSat AND LSTN
  * Can get time and people support but not money
  * Might make it easier to pursue internal NASA funding, as well
* Meeting with Pierros on Monday about versioning and release and LSTN funding
