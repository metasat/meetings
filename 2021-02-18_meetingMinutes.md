# 18 February 2021 MetaSat Meeting Minutes

**Time:** 15:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Vasilis Tsiligiannis, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* Potential funding: Working with [STAR Net](http://www.starnetlibraries.org/) to apply for NASA ROSES grant
  * [Proposal](https://nspires.nasaprs.com/external/solicitations/summary.do?solId=%7B7E892B3A-4F62-AEB4-6A08-9DCA1977F31A%7D&path=&method=init) period has opened
    * [Specific proposal](https://nspires.nasaprs.com/external/solicitations/summary.do?solId=%7B70022F64-A0FD-0B3D-7FFA-FCC298C068E6%7D&path=&method=init) we might pursue; [more specific details](https://nspires.nasaprs.com/external/viewrepositorydocument/cmdocumentid=807870/solicitationId=%7B70022F64-A0FD-0B3D-7FFA-FCC298C068E6%7D/viewSolicitationDocument=1/F.6%20SciAct.pdf) (PDF)
  * Potential hangup: since LSF is not US-based, might be more difficult to fund SatNOGS aspects, but we should be able to swing it
  * First due date: Mid-April
* Working on setting up Weblate 
  * Have an idea of what the repo will look like, just need to iron out the details
  * Moving back and forth between PDF and XML is going smoothly! 
* Talked with librarian from Santiago, Chile
  * Set up a Whatsapp group so we can communicate more smoothly
* Both Chile and Cambridge should be able to get their ground stations up this spring!

### MetaSat Updates 

* Got in contact with [Celestrak](https://celestrak.com/) (Dr. T.S. Kelso)
* [NASA's Open Data Portal](https://nasa.github.io/data-nasa-gov-frontpage/): might try to make a JSON-LD file of a mission that has data here that could be pointed to
  * This would go into the MetaSat paper, in addition to JSON-LD file for CERES mission
  * Also looking into their [API](https://data.nasa.gov/stories/s/gk8h-th3y)
* Ideas for vocab changes for next release going into GitLab issues this week
* Made a list of potential new crosswalks, too
  * Also list of related projects to maybe put in the website resources
* About ready to let students start helping with Wikidata items

### LSF Updates

* Using archive.org free hosting, but they offer paid hosting too
  * Could include in Sloan proposal
<!--
### Wolbach Updates

* x
-->
