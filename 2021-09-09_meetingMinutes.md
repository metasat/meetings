# 9 September 2021 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Nikoletta Triantafyllopoulou, Allie Williams  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; LISA - Library and Information Services in Astronomy)*  
<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  

### LSTN Updates

* Working on LSTN paper for LISA procedings
* Christian in Chile asked Nico to present on LSTN and Phaedra in October (citizen science and libraries in general)
* Book by Association of Research Libraries (Europe) about citizen science in libraries; Nico writing about LSTN
* People still working on Weblate
  * Someone has added to the [Glossary](https://hosted.weblate.org/projects/library-space-technology-network/glossary/) section (which we haven't used)
    * Anyone can add to the glossary, but since it isn't connected to GitLab, we can't directly see editing history
* Finishing up final social media report for LSTN

### MetaSat Updates  

* Working on Wikidata project, and with Overleaf

<!--### LSF Updates

* x-->

### Wolbach Updates

* Report for Sloan funding due by the end of this month; will need a report from LSF to include in it
