# January 23, 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Pierros Papadeas, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #10 | Contact potential partner libraries | Finalizing agreement with library in Mauritius | NC |
| #34 | Draft LSTN handbook | No update | NC |
| #36 | Research radio reception laws in Moldova | No update | PP |

<!--
**All other actions were completed or are no longer relevant.**
-->

### Logistics  

* [Small Satellite Conference 2020](https://smallsat.org/)
  * Call for exhibitors open for the next week
  * Also call for abstracts for presentations and posters
  * Will probably submit proposals for both

<!--
## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #38 | ---------------- | --- |
-->

## Updates

### LSTN Updates

* Educational meetings with [MIT](https://www.media.mit.edu/projects/democratizing-science/overview/)
  * Idea for collaborative ground station build using a [zine](https://guides.lib.utexas.edu/c.php?g=576544&p=3977232) for instructions/other info
  * Trying to figure out [learning objectives and activities](https://docs.google.com/document/d/1mtGxssZlLefq6AF14jTsdZ9A681aQ_2EIh9NgedAuKU/edit#)
    * Activities should be accessible regardless of tech at libraries (even if they don't have computers)
  * Want to brainstorm about new educational activities, how we want to interact with partner libraries
* [Mahatma Gandhi Institute](https://www.mgirti.ac.mu/) in Mauritius is very close to being our next partner library
* Scheduled library kickoff meetings
  * Virtual meetings with heads of each partner library; scheduled with [Marathon](http://www.marathonpubliclibrary.org/) and [Moldova](http://www.hasdeu.md/) so far
  * Moldova&mdash;can't ship ground station materials to US embassy (will not accept lithium batteries)
    * Will be trying Fedex instead

### MetaSat Updates

* Put together a draft [JSON-LD file for the LSTN kit](https://gitlab.com/metasat/metasat-schema/blob/master/Examples/LSTN_KIT_EXAMPLE.jsonld)
  * Includes granular information about internal and external hardware for the kit
  * Will be edited this week, so expect updates
* Still planning on February 14 release for v1.0
  * Lots of updates with "Ground Station" section right now
  * [Table view](https://docs.google.com/document/d/1Y3k76q2sRrjsrxJCjIVdIpzlpc-kD1dvFs8KNO_H1tE/edit) is most up-to-date right now
  * Next step will be updating "Space" section

### LSF Updates

* Coming up &mdash; finishing up hardware for PocketQubes!
  * Can we describe using MetaSat?
    * Will arrange meeting to collaboratively edit a JSON-LD file
    * This way we won't have to randomly pick a satellite for example
* Proposal to make [SatNOGS DB work with WikiData](https://gitlab.com/librespacefoundation/satnogs/satnogs-db/-/merge_requests/447)

### Wolbach Updates

* NASA meeting follow-up &mdash; Progress on Space Act Agreement

