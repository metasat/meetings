# 12 March 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #34 | Draft LSTN handbook | Ordered custom three-ring binders&mdash;will start with an intro and additional chapters sent out over time | NC |
| #36 | Research radio reception laws in Moldova | Trying to get a meeting with librarians to talk about it, but no update right now | PP |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #40 |  | XX |
-->

## Updates

### LSTN Updates

* Ground station build activity idea: Making cards for each ground station part so people can learn more about them and how they fit together.
  * If the activity works well, will mail out cards to collaborating libraries
    * Since we're planning to give them custom 3-ring binders, they can keep the cards in pocket of binders
* [Wolbach ground station](https://network.satnogs.org/stations/1378/) is up!
  * Quick question about noise levels
  * Night observations have less noise. Can anything be fixed for daytime noise levels?
  * [Daytime example](https://network.satnogs.org/observations/1836514/)
  * [Nighttime example](https://network.satnogs.org/observations/1847946/)
  * Might want to try changing the antenna gain to see if that helps
    * It looks pretty low right now, try increasing it
* Have also noticed that hex data can look pretty random, but sometimes ends with a string of repeating digits. Why might this be?
  * [Example](https://network.satnogs.org/observations/1847954/), this ends with many repeated "E3" digits.
  * Might be a demodulator issue.
    * Also, this observation should be vetted "bad," no signal in the waterfall (this was automatically vetted, and a false positive; try manually marking as bad)
* Is there a way to automatically schedule observations?
  * There is an auto-scheduler, but Fredy doesn't know a lot about it; try asking Pierros or Vasilis instead

### MetaSat Updates

* Hoping to launch new website today!
  * Adding database for ground segment terms and crosswalk
  * Right now, [test.schema.space](http://test.schema.space/), but should be moved to [schema.space](https://schema.space/) tomorrow
* Otherwise, doing initial research for space segment

<!--
### LSF Updates

* x
-->

### Wolbach Updates

* We can't bring in the LSF to Cambridge Science Festival next month
    * [Restrictions on travel](https://www.harvard.edu/coronavirus/travel-guidance) due to the coronavirus
    * The CPL ground station build SHOULD still be in April
