# 10 December 2020 MetaSat Meeting Minutes

**Time:** 15:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Fredy Damkalis, Pierros Papadeas, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; OSCW - Open Source CubeSat Workshop)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  

### LSTN Updates

* OSCW: Creating an Intro to LSTN presentation (about how we chose libraries, what we want for the pilot, what we've done so far)

### MetaSat Updates

* OSCW: working on MetaSat update presentation
* Archiving the GitLab repo next

### LSF Updates

* Working on a [milestone](https://gitlab.com/librespacefoundation/satnogs/satnogs-db/-/milestones/24) to have MetaSat working with SatNOGS
  * In talks about how to work with MetaSat versioning in the future
  * Coming up with a MetaSat versioning schedule is one of our next steps! (after first paper and release)
  * Not concerned as much about versioning itself as how the database is set up so that versioning does not impact it much
    * Each version of MetaSat should be backwards compatible
    * Don't have an API for MetaSat (yet!) or any hierarchy so should be straightforward for now
    * Don't have tooling to serialize RDF yet but that's a next step, and an API should be easier to fold in after that.
  * Will SatNOGS cover ALL of MetaSat or just a SatNOGS-specific subset of terms? Not clear yet.

### Wolbach Updates

* Looking into more potential grant sources
    * NASA [Universe of Learning](https://www.universe-of-learning.org/)
    * CfA is already a partner!
