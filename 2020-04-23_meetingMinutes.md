# 23 April 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Pierros Papadeas, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Vasilis Tsiligiannis, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #41 |  | XX |
-->

## Updates

### LSTN Updates

* Sloan is willing to extend timeline for the LSTN ground station builds
  * Extra 3 months; we are not sure we can do any LSTN stuff over the summer
  * Ground station installations will probably happen in the fall
* [LSTN handbook](https://drive.google.com/file/d/1cZDEFlQMzLN2_n9yPeS1uPqZHGQ_mfSP/view?usp=sharing) version 1 is complete
  * This is the first version that will be mailed out
  * Future chapters will be mailed out as they come
  * Will probably put the PDF on the LSTN website and archive on Zenodo, too
  * Getting ready to send to Moldova and Marathon, TX next week; hopefully Chile soon after

### MetaSat Updates

* Daina and Pierros followed up about [timeline](https://docs.google.com/spreadsheets/d/1qwSHVx6VUipPeqC4j3HCotBMdr7qc5RNqRcrA6MF4Q8/edit#gid=0) for implementation
* JSON-LD validation?
  * Right now, not much tooling, which we want to create
  * Closest thing to validation is JSON-LD playground, has its flaws
* Space segment update
  * Close to finishing terms
  * Will share spreadsheets; feedback?
    * [Electrical power systems](https://docs.google.com/spreadsheets/d/1Yk0V4fRPI19fgcKf0SuG0Vth8xg-3SL_XERBcVe9090/edit?usp=sharing)
    * [Attitude Control Systems](https://docs.google.com/spreadsheets/d/13SXWlzd-fD5Ixjg4cOQGA4jQiR7YTizhu05sggIXBB0/edit?usp=sharing)
    * [Propulsion systems](https://docs.google.com/spreadsheets/d/1Yk0V4fRPI19fgcKf0SuG0Vth8xg-3SL_XERBcVe9090/edit?usp=sharing)
  * A lot of weeding already done
    * Just removed some EPS elements that were too granular
    * We don't want set of elements to be too large
* Want to create a MetaSat JSON-LD export tool for the website
   * Similar to CodeMeta generator, but using recommended elements first, based on our database
   * Might also start making an ontology
* Want to prioritize SatNOGS-useful URIs and examples (like comms system) for now

<!--
### LSF Updates

* x

### Wolbach Updates

* x
-->
