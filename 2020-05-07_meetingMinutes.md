# 07 May 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Pierros Papadeas, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #41 |  | XX |
-->

## Updates

### LSTN Updates

* LSTN kit update: Waiting for FedEx to set up an account with Harvard so we can pay for international shipping
  * Texas received their kit! All safe and accounted for, nothing broken as far as we know.
  * Also sent handbook to Cambridge Public Library for feedback
  * Envisioning an expanded role for Cambridge Public Library for the future&mdash;currently in the early planning phases

### MetaSat Updates

* Update to our [website](https://schema.space/metasat)&mdash;Many new URIs have been minted
  * Most space segment terms have been minted, except those for the communications system
  * Added some new UI stuff - search, sorting
  * Examples and synonyms on term pages are pulled from real world examples so should be accurate - let us know if you see any mistakes
* Challenging areas to make metadata for the future: 
  * Software (such as SDR elements)
  * Datasets (https://schema.org/Dataset)
  * Computers (such as RPi or other OBC, or PDU, telemetry-related computers, etc)
* Next steps: finishing up JSON-LD files and working toward adoption
  * For website, will make a formal crosswalk and reinstate the sorting tool
* After major JSON-LD file changes, will start submitting issues before making changes again
* Might add an ontology in the future, but that's not the priority right now
  * No hierarchy to terms right now, except in JSON-LD files. Will maybe add visuals for each LD file to make hierarchy more evident
  * This will be more for outreach and explaining and presentations
* ["Terms" files](https://gitlab.com/metasat/metasat-schema/-/tree/master/Terms) - what is the best way to describe these?
  * They aren't example files because they are empty. They aren't templates because they are missing potential elements that could be used (to avoid repetition). They are not minimal element sets because they include mutually exclusive terms (such as the electrical power systems file including terms for both solar and nuclear power).
  * "Terms" is probably not a good name for the folder
    * Maybe "groupings of subsegments," or describe as "Partially complete frameworks to get you started"
    * "Term sets" or "element sets" for the space segment
  * Will need to update README to describe terms vs. examples folder
    * Example files are not empty, they contain metadata about real examples. Terms files do not.
  
  
### LSF Updates

* LSF might to start implementing MetaSat on Monday
  * For the DB implementation
  * Not on artifacts side, which requires more observation elements
  * Want to start by running as a test on existing stations
    * Might connect these station owners with Daniel
  * In the future, stations can "opt in" to exporting MetaSat compatible data
  * Eventually folding in with the HDF5 tests, which have already started

<!--
### Wolbach Updates

* x
-->
