# 8 April 2021 MetaSat Meeting Minutes

**Time:** 15:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, <!--Eleftherios Kosmas,--> Pierros Papadeas, Nikoletta Triantafyllopoulou, <!--Vasilis Tsiligiannis,--> Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* LSTN blog post&mdash;should have an update next week
* Weblate update
  * Website - still working on making a Weblate tutorial for translators
    * Hoping to have a rough draft to share around within the next week!
  * GitHub response
    * Heard back from Weblate people on [GitHub](https://github.com/WeblateOrg/weblate/discussions/5771#discussioncomment-578922); confirmed that they do not have a tutorial for translators but are open to collaborating on one
* LSTN kit
  * Replaced Lime SDR with RTL SDR; Noise is down and pulling down more packets. Seems successful!
    * [ELFIN-A observation](https://network.satnogs.org/observations/3910804/)
    * [SpooQy-1 observation](https://network.satnogs.org/observations/3907084/)
      * [Another SpooQy-1 observation](https://network.satnogs.org/observations/3810596/) for comparison
    * Never decoded this much data from these satellites before! Waterfalls look pretty nice.
    * Gain is set pretty high
  * More ideas that might help:
    * Move the station farther away from the antenna; need a longer cable
    * Then change power from POE to DC line, or change RF cable
* Marathon, TX ground staiton troubleshooting; might use a non-static IP address; will this cause issues? Shouldn't, most stations work like this. (DHCP)
  * Wifi might be complicated in places like universities where devices must be authenticated; not as much of an issue at other places, where you can connect freely or just need a password, for example
  * Talk to other libraries about their setups; we want to get an idea about what advice to give to different potential partner institutions

### MetaSat Updates 

* Still working on the MetaSat paper
* Also working on the MetaSat generator tool! 
  * Can import existing structured JSON-LD, fill it out, and spit out filled out JSON-LD with data
  * First step to exporting JSON-LD structured how a user wants it!
  * Might get a demo online soon, might invite Becca to this meeting next week
* Australian Research Data Commons update: Should find out about hosting MetaSat next week!

### LSF Updates

* Not much progress yet; did some testing and debugging
  * Is the issue because of JSON-LD? No!
    * Just want to see if it's difficult to work with
    * [JSON-LD validator](https://github.com/caltechlibrary/convert_codemeta)

### Wolbach Updates

* NASA ROSES proposal coming along
  * Should have a budget ready for internal review soon
  * Anticipating draft of full proposal to share within a few weeks; want to finish by the first week of May
  * Final NOI draft shared
    * Submitting next Wednesday, so feedback by then please!
  * Adding proposal to buy stations from LSF, and will give them the room to formally research what the best parts for an affordable, accessible kit
