# 5 November 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Pierros Papadeas, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  

### LSTN Updates

* Have had some issues with our LimeSDR on the Cambridge station, have to reboot the computer every time
  * Is there a better way?
  * SatNOGS is aware of the issue and they should have a fix for this issue soon...working on debugging right now
  * Also having a noise issue
    * Best solution is to move the RPi away from the antenna; might include a longer RF cable in future LSTN kits
    * Also doing a little bit of experimenting to see what small tweaks will help this problem
    * Moving the LNA closer to the antenna mayb also help 
    * Can this all go in one box? Maybe. Some LNAs are self contained, have their own weatherproof box so won't need another
* ARDC proposal: In the queue waiting for final go-ahead from SAO admins

### MetaSat Updates

* [PMPedia](https://pmpedia.space/) update: Knowledge base for spacecraft parts 
    * Unlike SPOON, which is for assemblies (parts put together) while PMPedia is for smaller parts
    * Also includes ground system and ground station stuff
    * Daniel met with some of the people from it
        * They are thinking adopt MetaSat for their database
        * May specifically adopt in JSON-LD
* Updating pages for MetaSat website
    * Want to make them shareable so will hopefully be giving those out next week 
    * Track changes in Google docs is messy and hard to read, so may just put them on GitLab and then send out a link on Riot

### LSF Updates

* Still working out kinks about satellite ID

<!--
### Wolbach Updates

* x
-->