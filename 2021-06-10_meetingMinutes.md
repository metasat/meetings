# 10 June 2021 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Nico Carver, Daniel Chivvis, Katie Frey, Nikoletta Triantafyllopoulou, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* 10 languages added to Weblate! [Chapter 1](https://hosted.weblate.org/projects/library-space-technology-network/lstn-handbook-chapter-1/) is fully translated in traditional Chinese and Romanian
    * How do we keep people engaged when things are fully translated?
        * Traditional Chinese has been fully translated, for example, so how can someone who translates in Chinese help out more?
            * Can check failing checks, translate in different writing standards (simplified or pinyin Chinese), wait until we add more material to Weblate (that might not be for several months to a year, though)
* Posted about translation project from LSF accounts about 10 days ago, and a few days ago from SatNOGS accounts
    * Both communities seem like they are embracing the project, especially SatNOGS
    * Can Nikoletta contact partner libraries asking to share translation stuff?
        * Yes! Nico will email them today to ask if he can share their contact info.

### MetaSat Updates 

* Still working on paper, GitLab issues, etc.

<!--### LSF Updates

* x

### Wolbach Updates

* x-->
