## LSF Kickoff Summary
### Day 1
Primarily discussed logistics, MetaSat, SatNOGS as it currently exists, and limitations to the current metadata architecture -- focus should not be too tied to what exists currently though as LSF is very open to making changes. Day 2 will focus more on clearning up understanding of current systems, ground station installation logistics for LSTN (Nico's [document][introToLSTN] and diagram), and future work.

Logistics  
* Pierros will be the primary contact at LSF
    * Hiring will be done based on software engineering skills needed to enable implementation
* Weekly meeting via Mumble (or other audio call platform)
    * Will find ideal regular meeting time via Doodle
* GitLab for shared documents/issue tracking
    * MetaSat will have its own GitLab group
        * GitLab issues will be used to update the SatNOGS wiki as appropriate
        * Action items should be added as GitLab issues so they can be tracked and incorporated into SatNOGS development plans
* Research notes, article drafts, and other docs will be added to Wolbach's drive and published openly
    * Need to find out if we can get LSF access to our shared drive -- Would giving them Harvard Library credentials give them access?
* SatNOGS forums (Riot) for chat with LSF
    * Use Slack for participating in/learning about other related projects (e.g. the ESIP channel)

Action items - day 1  
<!-- These are the kinds of things we would add as "issues" to GitLab -->
* Created a basic diagram of the existing SatNOGS pipeline
    * [Diagram draft][diagram] - LSF needs to give feedback
* Pierros will share the existing ERD for SatNOGS DB
* We need to settle on domain names for MetaSat and LSTN that is independent from LSF and Wolbach
    * Wolbach landing page is great but need to have a neutral link until some kind of governance situation is established (post-pilot)
* We need to put together an email list
* We need to select a tool if necessary for schema management

MetaSat priorities in the next three months  
* **Develop use cases** - what use cases does the current metadata architecture not accommodate and why?
    * Example: Currently have a predefined set of metadata elements from "artifacts" but more flexibility is needed to represent what an "observation" is
      * Solution could possibly mirror the approach taken by STSci with MAST: [DOI minted for a user defined set of objects from MAST archives][DOI]
      * Not currently able to define "types" of missions but "service mapping" element could help with this (service mapping not generalizable outside of SatNOGS though)
    * Need to determine what use cases are specific to SatNOGS and will be needed for implementation vs. use cases that would be general for the schema
    * Use cases will be shared via GitLab
* **Identify metadata elements** that could be defined using existing standards
    * What needed elements are missing entirely that we may need to define?
* **Start developing prototype schemas**
    * Define questions to ask about these at OSCW workshop
* **Develop a survey draft** for satellite operators and potential implementers
    * A separate survey will be useful for novice community

MetaSat in the next six months  
* **Complete prototype schemas**
    * They should be ready for external feedback and meet priority use case needs
* **Circulate survey**
    * Should be used to make decisions about limitations of prototypes
    * Will inform software engineering needs
* **Define questions for further focus groups**
    * What are we unable to gather from surveys should be addressed during focus groups

----
### Day 2  
Primarily discussed LSTN and projects that LSF has been thinking about (or is near completing) that may support LSTN. We also settled on creating a ground station kit for LSTN participants, rather than giving the groups options. We also finished talking through logistics.

Logistics (cont.)  
* The GitLab group has been created
* LSF has experience working remotely to support ground station builds and will help pilot libraries using email, audio call, video call if needed
    * LSF will contribute to materials sent to pilot participants to support community builds.
    * The LSTN kit will be an S-band station made with components that will help minimize uncertainty
      * SatNOGS "reference kit" is already in demand and LSTN could essentially test out a version of the kit
* Clarified that funding is available to support potentially unanticipated aspects of installations

Action items - day 2  
* Wolbach staff need to create GitLab handles and send them to Pierros
* Wolbach will share list of countries where we might install pilot stations
    * Feedback from LSF could help narrow down locations
    * LSF knows of groups that might be helpful in supporting the libraries
* Wolbach will update Nico's LSTN document and ask LSF for feedback
    * LSF also willing to give feedback on future documents sent to potential pilot participants
* We still need to determine the most reasonable station to install at Wolbach
    * Perhaps omnidirectional X-band

LSTN priorities in the next three months  
* **Finish LSTN invitation**/information documents
* **Select and prioritize potential public libraries** for installations
    * Reach out to potential local support to ensure they would be willing to help if needed
* **Send final invitations** to potential participants
    * Get participation commitments from 3 locations
      * Create commitment document (?)
* **Begin gathering information about pilot libraries** to determine the best approach to getting kit components to pilot locations
    * Availability of different parts around the world varies
    * May decide Wolbach purchases kit components and send that directly to participants
* **Continue developing ground station kit**
    * Estimate total price of an individual kit
    * Begin complaining <!-- compiling? --> list of tools needed for assembly

LSTN priorities in the next six months  
* SatNOGS team **completes LSTN ground station kit**
    * Determines best approach to purchase/send kits to locations
* **Documentation started** to support community builds
    * Feedback from CPL would be helpful
* **Logistic planning** begins with pilot locations



[introToLSTN]: https://drive.google.com/a/cfa.harvard.edu/file/d/0B9JtbWOLepvgekhiS0gxd0twOHQ3LWZ3U19sLXFVQ01SaDRV/view?usp=drivesdk
[diagram]: https://www.lucidchart.com/invitations/accept/2cff076d-3d43-429d-9d3b-cfd68729592e
[DOI]: https://archive.stsci.edu/doi/search/
