# 09 July 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |
| #41 | Add MetaSat primer to schema repository | README has been updated, will add a "Docs" folder soon | AW |
| #42 | Make plans for updating website | Working on wireframe for "Resources" page | AW |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates

### LSTN Updates

* FedEx update:
  * Hopefully librarians in Chișinău, Moldova will be able to pick up their kit today!
  * Chile still up in the air; asking librarian to contact FedEx
* Want to work on online resources for libraries to use during shutdown
  * What about language barrier?
    * Might talk to Spanish-language satellite team - [Quetzal](https://space.skyrocket.de/doc_sdat/guatesat-1.htm) satellite team is active on SatNOGS
    * Considering FB Live interviews or talks by Spanish speaking team

### MetaSat Updates

* [README](https://gitlab.com/metasat/metasat-schema/-/blob/master/README.md) is updated and will add more documentation soon
  * Will add more documentation resources in another folder soon - what should it be called?
  * For now, will put in a folder called "Docs," but might change
* Could SatNOGS add a "MetaSat" page on their wiki? To point to schema.space and our repository
* Getting ready to ask for feedback
  * Looking into instrumentation and more areas that have been missed in the past
  * More stuff for crosswalk, too
* SmallSat meeting [poster](https://drive.google.com/file/d/1AUgFTcPe4Zc6SAfCPrB1k9jDTLpuxQFV/view) is ready
  * Will be a virtual poster session online

### LSF Updates

* Working on browsable, visual JSON-D
  * [API page](https://db-dev.satnogs.org/api/)
* Working on artifacts, too; hopefully meeting about it within the next week

### Wolbach Updates

* Writing followup grant - working to define some potential new positions to be funded
