# October 04, 2019 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Vasilis Tsiligiannis, Fredy Damakalis, Manthos Papamatthaiou  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; OSCW - Open Source CubeSat Workshop)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | --- | --- | --- |
| #7 | Attempt to obtain radio license in Namibia/Zambia | Waiting for update | PP |
| #10 | Contact potential partner libraries | Haven't heard back yet; Did get a email back from Woksape Tipi, no commitment; may contact Namibian libraries soon | NC |
| #19 | Test LSTN kit | No new update yet; scheduling observations | PP |
| #21 | Draft list of stakeholders to contact | (After OSCW) | DC, AW |
| #22 | Email specific individuals who may want to give feedback on MetaSat schema | (After OSCW) | DC |
| #30 | Draft emails to LSF contacts; run by LSF | (After OSCW) | DC |
| #31 | Confirm workflow for changes in schema and sharing changes with Libre Space | (After OSCW) | DB |
| #33 | Draft LSTN document: What libraries can do with a ground station | "This probably won't be a standalone document, but instead part of a larger LSTN handbook that will keep evolving." Nothing written up to share yet. | NC |

<!--
**All other actions were completed or are no longer relevant.**

## Action Items

| Issue number | Summary | Assignee |
| --- | --- | --- |
| #34 |  |  |
-->

## Updates

<!--
### Logistics  
*
-->

### LSTN Updates

* The "What Libraries Can Do With a Ground Station" probably won't be a standalone document, but instead part of a larger LSTN handbook that will keep evolving.
  * What do people think of these headings in this order?:
    * Build
    * Listen
    * Learn
    * Collaborate
    * Develop
  * Will ask for more feedback when a draft is ready

<!--
### MetaSat Updates

* x

### LSF Updates

* x
-->

### Wolbach Updates

* OSCW - Open Source CubeSat Workshop - October 14-16 in Athens
  * Want to make the official MetaSat announcement here
  * Planning on contacting potential collaborators/stakeholders afterwards

<!-- Links -->
