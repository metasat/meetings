# 25 February 2021 MetaSat Meeting Minutes

**Time:** 15:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Pierros Papadeas, Vasilis Tsiligiannis, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* Have been in contact with librarian from Biblioteca de Santiago
  * He says the ground station is installed; Next step: Get it online
* Working on [LSTN translations](https://hosted.weblate.org/projects/library-space-technology-network/) on Weblate
  * Handbook is mostly ready, have a few more chapters to add
  * Planning on requesting libre hosting this week
  * Right now it's set up so anyone can add a new language, should we keep this?
    * We have a cap of 60 languages
    * This shouldn't be an issue!

### MetaSat Updates 

* Working on MetaSat paper, incorporating real-world examples
  * Was using CERES mission, however, does not have open data
  * Because of this, we want another example file as well, for the [MINXSS](https://lasp.colorado.edu/home/minxss/) project out of UC-Bolder
    * Also there are some CfA people involved in this project!
  * Might make a new repo for the paper materials.
  * Example files include concepts that are not in the MetaSat vocabulary yet
    * For example, want to change how we handle talking about orbits
      * Might want to talk with Jonathan McDowell about this
* MetaSat issue templates; put in new folder in repo?
  * These should be up by the end of the week

### LSF Updates

* Found contacts at archive.org, Portico and Clocks for potentially hosting SatNOGS material
* Also finding UN contacts to meet with
* Working on MetaSat integration with SatNOGS! Should have news next week.

### Wolbach Updates

* How to broadcast about our updates (issue templates, LSTN handbook on Weblate for example)
  * Might make a SatNOGS blog post about these things
  * Will talk more about this next week
