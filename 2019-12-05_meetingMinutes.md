# December 05, 2019 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Pierros Papadeas, Nico Carver, Daniel Chivvis, Allie Williams, Vasilis Tsiligiannis, Fredy Damakalis   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #10 | Contact potential partner libraries | Updates below; our first official partner is in Moldova! | NC |
| #22 | Email specific individuals for feedback | No update  | DC |
| #34 | Draft LSTN handbook | Working on this, potentially making it a [webpage](https://lstn.wolba.ch/build/single-board-computer/) instead? | NC |
| #35 | Make decisions on LSTN laptop | No decisions yet; will go with buying the parts (will end up on website Nico is making) | DB, NC |
| [MetaSat Schema 4](https://gitlab.com/metasat/metasat-schema/issues/4) | LSF: give feedback to Daniel on new schema | No update; new schema is up! | -- |

<!-- **All other actions were completed or are no longer relevant.**


#### Logistics  
* x


## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #36 | ---------------- | --- | -->

## Updates

### LSTN Updates

* Partner libraries
  * We have our first official partner library! In Moldova.
    * Lots of public programming at this library.
    * In the capital city of Moldova--Chi&#x219;in&abreve;u
    * One of 26 branches in the capital
  * Waiting for formal, final agreement from Namibia
  * Rethinking what to do in rural US
  * Also still meeting with MIT, making plans on the educational front
* LSTN educational [website](https://lstn.wolba.ch/build/single-board-computer/) draft, might use this instead of making a more private handbook just for the libraries
  * Can we get feedback from LSF? (Or give them editing privileges)
  * The site still needs LSF/SatNOGS/Wolbach branding
* Potential changes to the ground station kit
  * Part of the ground station kit is soldered together--this isn't user-friendly/practical
  * LSF was experimenting with LNA and bandpass filter--think they've found a good, reliable component that combines the two
    * Currently being used by Station 977
  * Might change kit to reflect both the new LNA/bandpass filter (which is USB powered) and a new antenna.
  * As the LSTN kit continues to fluctuate and change, can we gain a person/cover someone's time to keep the kit up-to-date?
  * For now, Wolbach is going to buy the pieces and make any necessary modifications (drill holes, etc), but need a new plan to scale up.

### MetaSat Updates

* Version 0.1.8 is newest--only has a few changes
  * Got feedback from conferences, Thibault Gateau of NanoSpace database, and the LibreCube specification
  * For next version, will rearrange orbital elements, feedback from Juanlu of [Poliastro](https://github.com/poliastro)
* Copyright and attribution for MetaSat
  * [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/), Creative Commons ShareAlike license
    * Any feedback? MetaSat isn't considered open source software in US
    * It does not have a license on it right now
    * Goal: Anyone can use it but need to attribute. Would be open to be used for commercial use, only with attribution

### LSF Updates

* Need to start thinking about transferring their models to MetaSat--can they make it completely MetaSat? Will they need to change their database?
* Want to try exporting first, then importing. In parallel, what parts of the database need to be changed?
* Feasible right now, just need to extend API, implement in Django models
* If MetaSat is a living, breathing standard, how do they keep up with changes?
* How do we make sure that the schema keys match with the SatNOGS database?
  * There are algorithms for that--Expansion and compaction. Find them at json-ld.org, scroll down to "Developers."
    * Will be updating the MetaSat Schema repository's README with an explanation of these algorithms; in the meantime, [this blog post](https://blog.codeship.com/json-ld-building-meaningful-data-apis/) and [this video](https://www.youtube.com/watch?v=Tm3fD89dqRE) explain it well.
  * Algorithms already exist in Javascript, Python, PHP, Ruby, Java, C#, Go, Erlang/Elixer. All of them work with JSON-LD v1.0, and are currently being updated to work with JSON-LD v1.1
    * The Ruby implementation is already fully updated to work with 1.1. JavaScript will probably be next, followed by Python.

### Wolbach Updates

* AAS MetaSat meeting in January--will solidify feedback after that, to make a "stable" schema (v1.0) with minor changes. Aiming for releasing 1.0 by the end of January.
  * At that point, figure out workflow to keep it up to date, and implement in SatNOGS
  * We will continue to make new releases as small changes build up
