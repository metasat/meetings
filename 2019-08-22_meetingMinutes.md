## 08 22, 2019 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Pierros Papadeas, Nico Carver, Daniel Chivvis, Allie Williams, Vasilis Tsiligiannis, Manthos Papamatthaiou
**Meeting purpose**: Updates from both teams

*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; ERD - Entity Relationship Diagram; BOM - Bill Of Materials [i.e., the LSTN kit materials]; UUID: Universally Unique IDentifiers; SiDS: Simple Downlink Share convention)*

#### Action Review  
* [Issue #6][LSTN]: Update LSTN kit document
    * Nico will work on updating document to send to potential partner libraries
* [Issue #7][licensing]: Send Wolbach info on intl licensing
    * Continuing to research licensing in Namibia and Zambia
    * Next step--applying for international license in Namibia
* [Issue #10][shortlist]: Wolbach contacting potential partner libraries
    * Planning to contact Woksape Tipi and Biblioteca da Floresta this week
* [Issue #13][website-outline]: Share feedback/ideas on website outline
    * Ongoing
* [Issue #14][website-update]: Update website ([schema.space](https://schema.space))
    * Next steps: adding a forum, deciding what information to host where
* All other actions were completed or are no longer relevant.

<!-- #### Logistics  
* Mumble seems to work fine now! -->

#### Action Items  
* Send Wolbach SatNOGS [Network database ERD][network-ERD]
    * This is separate from the ERD that has already been shared
* [Feedback][schema-feedback] on current schema ([metasat.schema.space](https://metasat.schema.space))
* [Move code for website (dendrogram tool) from GitHub to GitLab][dendrogram]
    * New repository in Metasat project
* Continue [researching Eastern hemisphere libraries][Eastern-libraries]
  * If necessary, find backup countries/libraries
* LSF: [test current LSTN kit][kit-test]
* [Open GitLab Wiki, add ERD figures][wiki-erd]

#### SatNOGS ERD Walkthrough  
See figure [here][satnogs-db-erd]  
* Throughout figure: Required fields are darker
* Left side: Internal information (pertaining to users, etc)
* Right side: More relevant information for MetaSat
    * Satellite info, telemetry, transmitter, demodulated data
* Currently working on updating some of these fields/relationships
* Currently no info on mission model--want to add
    * Because mission can have many satellites
* On identifiers:
    * Each transmitter currently has a unique, randomly generated UUID
    * Ideally, satellites will have unique ID, too
    * Satellite IDs are an open issue in the field--what is the best way for everyone?
    * Both US entities and ESA are trying to find solutions; ideally a non-profit will solve this issue
    * Daina saw a talk about [an interesting potential solution][cubit] at SmallSats Conference
* One-to-many relationship between satellite and transmitters
    * Transmitter information fully fleshed out because it is important for the SatNOGS database
    * UUID for each transmitter--these are randomly generated and unique
* Mode: Connected to transmitter--stores info on radio frequency modes of operation
* Demoded data: What observers get from SatNOGS: Observe data that comes from satellite and transmitter
    * Basically the data the satellites send in some format or another
    * Pipeline: User schedules observation, network receives observation, produces raw data that goes to SatNOGS DB in demoded, then decoded and pushed as decoded data, then dashboard visualizes the decoded data in some way
    * Some properties are outdated or set to be changed--for example, payload_frame stores information on a different way to store data (i.e., storing decoded data in a raw data model, instead of in an external database)
    * Based on [SiDS protocol][sids]

#### MetaSat Updates  
* Current, up-to-date schema is at [metasat.schema.space](https://metasat.schema.space)  
* Visualized as a dendrogram, based on software Katie made for another project.
    * May host a CSV or JSON file elsewhere
* Anyone can send feedback on the dendrogram if anything needs to be added, moved, etc.  
* Some fields still needs to be added: for example, information about ground stations  

#### LSTN Updates  
* **Radio licensing info**  
    * In Namibia, does owner of license need to be someone who can physically access the station? Not clear  
    * Next step: Try to get a license, see if we can get guidance from the ministry  
    * Next backup is Zambia, but it is harder to find information on libraries there  
    * Next step after that is to look for different country candidates in the Eastern hemisphere  
* **Contacting potential partners**
    * Wolbach will send invitations to potential partner libraries
      * Will probably start with domestic rural (Woksape Tipi) and Brazil (Biblioteca da Floresta)
      * Will wait for more information on Namibia and Zambia
* **Wolbach/MIT meeting**
    * Meeting next week; MIT is interested in library component
    * Will talk about potential educational opportunities for once libraries receive/install the kit
* **LSTN kit**
    * [Updated BOM][LSTN-BOM] has been shared
      * Minor changes may be coming
      * May need to add power converter
    * Antenna does come with mounting equipment (clamps)
    * Once libraries receive the kit, all that's missing is the mast
      * How high does mast need to be? Tall enough to clear obstacles by about 1.5-2m

<!-- Links -->
[satnogs-db-erd]: https://gitlab.com/metasat/meetings/uploads/8337b98ceeeb267898d30e6b56814b7c/satnogs-db-model.png
[cubit]: http://mstl.atl.calpoly.edu/~workshop/archive/2019/Spring/Day%202/Session%203/SamsonPhan.pdf
[sids]: https://www.pe0sat.vgnet.nl/decoding/tlm-decoding-software/sids/
[LSTN-BOM]: https://docs.google.com/spreadsheets/d/1lk7t_mQgBvG-s7zxE6ZbNbF9SjPQkaOvTbWxXiMYDLQ/edit

[LSTN]: https://gitlab.com/metasat/meetings/issues/6
[licensing]: https://gitlab.com/metasat/meetings/issues/7
[shortlist]: https://gitlab.com/metasat/meetings/issues/10
[website-outline]: https://gitlab.com/metasat/meetings/issues/13
[website-update]: https://gitlab.com/metasat/meetings/issues/14
[network-ERD]: https://gitlab.com/metasat/meetings/issues/15
[schema-feedback]: https://gitlab.com/metasat/meetings/issues/16
[dendrogram]: https://gitlab.com/metasat/meetings/issues/17
[Eastern-libraries]: https://gitlab.com/metasat/meetings/issues/18
[kit-test]: https://gitlab.com/metasat/meetings/issues/19
[wiki-erd]: https://gitlab.com/metasat/meetings/issues/20
