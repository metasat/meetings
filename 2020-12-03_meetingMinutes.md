# 3 December 2020 MetaSat Meeting Minutes

**Time:** 15:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Pierros Papadeas, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  

### LSTN Updates

* Finished [intro material](https://lstn.wolba.ch/introduction/)
  * Something early pilot participants asked for, a short presentation introducing satellites/LSTN
* LSTN website update, added a [resources page](https://lstn.wolba.ch/resources/)
  * More ideas and activities from others (like pilot participants) can be added here!

### MetaSat Updates

* Updated files in [Docs folder](https://gitlab.com/metasat/metasat-schema/-/tree/master/docs)
  * Would appreciate feedback! These will be going onto the website 
* External context - Update [example files](https://gitlab.com/metasat/metasat-schema/-/tree/master/examples) to point to [external context](https://gitlab.com/metasat/metasat-schema/-/blob/master/context.jsonld), and add information about it to [JSON-LD primer](https://gitlab.com/metasat/metasat-schema/-/blob/master/docs/json-ld_primer.md)
* Will be archiving the GitLab repository&mdash;more details later
* [PMPedia](https://pmpedia.space/) is still interested in MetaSat integration
* Meeting with someone from [CEOS instrument database](http://database.eohandbook.com/) about possible collaboration
* Have we talked with anyone from [eoPortal](https://directory.eoportal.org/web/eoportal/satellite-missions)? Not yet
    * Could be a good connection to make!

### LSF Updates

* MetaSat import feature coming soon?
* Having conversations about satellite ID - might reevaluate how IDs are assigned

### Wolbach Updates

* Want draft of new Sloan proposal this month
  * We need a roadmap
  * Should have a rough draft soon
* Haven't heard back from ARDC yet 
* [OSCW](https://oscw.space/) proposals: Yes, we are submitting! One for MetaSat and one for LSTN
