# 28 January 2021 MetaSat Meeting Minutes

**Time:** 15:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |
<!--
**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  

### LSTN Updates

* Crowdsourced translation options
  * [Weblate](https://weblate.org/), [Crowdin](https://crowdin.com/), [Transifex](https://www.transifex.com/)
    * All offer "Free for open source" plans
    * Would recommend Weblate, because it is open source and its "Free for open source" plan does not have the caveats the other services have
    * Can choose what languages to translate to, including dialects; get 60 with Weblate
  * Next step: Structuring strings into a [supported file type](https://docs.weblate.org/en/latest/formats.html?highlight=file%20type#translation-types-capabilities) and [signing up for open source project](https://hosted.weblate.org/hosting/) on Weblate
  * What will it take to convert file? Is there a script or converter that will be easy to repeat?
    * Should come up with something that is easy to repeat; as little manual work as possible.

### MetaSat Updates

* MetaSat paper update
  * Building a new example file, so that we have concrete examples to reference
  * Current examples do not demonstrate all of the functionality of MetaSat
  * Appendix will have example JSON-LD file, [tables](https://docs.google.com/spreadsheets/d/1LD9n5xymQqC-fRi6rE6OELkaLoXNCOfEHKJ-HOv2Y-c/edit#gid=0), crosswalks for an actual mission; [CeREs CubeSat](https://www.nasa.gov/image-feature/elana-19-ceres) out of Goddard
  * https://docs.google.com/spreadsheets/d/1LD9n5xymQqC-fRi6rE6OELkaLoXNCOfEHKJ-HOv2Y-c/edit#gid=0
* [Code of Conduct](https://docs.google.com/document/d/16qQ-fTwofHQynn5Cdbwf-S-6nyIbBKKjZGu1C9VFtM0/edit) has been shared, and contributor guidelines will be shared soon. These will be going up on the GitLab repo.
* [URLs and Semantic Web](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/URLs_semantic_web.md) doc on GitLab; next step is to put it on the website
* MetaSat Wikidata crosswalk
  * For concepts that already exist, MetaSat is crosswalked!
  * For example, on the [Wikidata page for antenna](https://www.wikidata.org/wiki/Q131214) scroll down on "MetaSat ID"

### LSF Updates

* Fixes on API endpoint

### Wolbach Updates

* Working on paperwork for grants, received more letters of support
