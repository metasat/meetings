## Sept 05, 2019 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Pierros Papadeas, Daniel Chivvis, Allie Williams, Vasilis Tsiligiannis, Fredy Damkalis  
**Meeting purpose**: Updates from both teams

*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; ERD - Entity Relationship Diagram; SAA: Space Act Agreement; CSV: Spreadsheet file format; JSON: Metadata file format; CI - Continuous Integration)*  

#### Action Review  
| Issue number | Summary | Discussion | Assignee |
| --- | --- | --- | --- |
| #7 | Attempt to obtain radio license in Namibia/Zambia | Pierros emailed amateur radio society and hasn't received a response; might directly contact ministry next | PP |
| #10 | Contact potential partner libraries | Contacted Woksape Tipi should hear back soon; Forest Library in Brazil has not responded | NC |
| #16 | Review current schema using website dendrogram and JSON file | Moved to "Doing" | FD |
| #18 | Research backup Eastern hemisphere libraries | Will do this week | NC |
| #19 | Test LSTN kit | Hardware is ready, waiting for software; waiting for the Raspberry Pi 4 | PP |
| #21 | Draft list of stakeholders to contact | Still working on it; we want to narrow down our list a little | DC, AW |
| #22 | Email specific individuals who may want to give feedback on MetaSat schema | LSF has list of 4 people Wolbach might not know who should be able to offer good feedback; all know LSF/SatNOGS well | DC |

**All other issues are complete or no longer relevant**  


#### Logistics  
* New "Done" board in issues; during the week, move issues to "Done," then after going over in the meeting, move to "closed."
* [New repository for schema](https://gitlab.com/metasat/schema-drafts) (Schema Drafts); can add schema-specific issues here

#### Action Items  
| Issue number | Summary | Assignee |
| --- | --- | --- |
| #28 | Add JSON/CSV to repository; Daniel needs permissions (schema drafts project) | DC |
| #29 | Set up CI to check/validate JSON file automatically in repository | VT |
| #30 | Draft emails to LSF contacts; run by LSF | DC |

*Also: anyone can open issues on new [schema repository](https://gitlab.com/metasat/schema-drafts), too, with questions, examples, suggestions, etc.*  


#### LSTN Updates  
* Haven't heard back from Forest Library in Brazil
    * Will probably reach out to back-up libraries
* Woksape Tipi responded, should hear back with more info within the next week
* Need to look into different Eastern Hemisphere countries
    * Radio licensing issues; see Issue #7 above

#### MetaSat Updates  
* Fredy has looked at [ERD definitions list](https://docs.google.com/spreadsheets/d/1CH3rUkEUH7m5nwkwFK3A8f5qBQlHU_IQvooIP5Zu7hU/edit?ts=5d711364#gid=0) and adding comments for clarity
* Fredy will be looking at schema (JSON file or dendrogram) and will add issues soon

#### LSF Updates  
* LSF looked over [website](https://schema.space)
    * Feedback: add something more descriptive to homepage (call to action, etc.)
    * Make the logos more balanced
* Also made a forum post soliciting feedback on schema
    * Some people looking at [schema](https://metasat.schema.space) already
    * Can give us feedback directly from the dendrogram

#### Wolbach Updates
* Meeting with NASA
    * Setting up regular meetings with Craig Burkhard now
    * Can work together even if SAA isn't finished yet
    * LSF can join in on phone calls
    * Connect LSF with Craig so they can figure out their international SAA
* Future meeting with MIT (Monday)
    * No formal agenda yet
    * What resources are needed for a robust curriculum?
    * Craig Burkhard at NASA will also join
    * Can contact Daina if there is anything she should bring up

<!-- Links -->
