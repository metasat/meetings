# Space Library Project - MetaSat and LSTN

This repository is used for organization and details meeting minutes and action items.

Meeting minute file names use a YYYY-MM-DD date format.

For more information about this project, visit us at [schema.space](https://schema.space).

## Ways to contribute

* [Use the sorting tool](https://metasat.schema.space/)
* [Submit an issue on GitLab](https://gitlab.com/metasat/schema-drafts)
* [Send us an email](https://schema.space/contact/)
