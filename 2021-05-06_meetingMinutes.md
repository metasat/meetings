# 6 May 2021 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Nikoletta Triantafyllopoulou, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* [Draft](https://docs.google.com/document/d/1CZw140I7-7Ghfe_1kLxEOttD6LS3-i-gKFGGns4QihE/edit) of blog post has been shared
  * Nikoletta will check feedback and make changes tomorrow
  * Might get rid of section on MetaSat, might want a separate blog post on MetaSat later, when we start asking for feedback
* We have ordered RTL-SDRs to see if we can update the LSTN kits with them
  * Talking with Chilean librarian on Monday to try to update their station
  * Also experimenting with a longer cable for the Wolbach ground station


### MetaSat Updates 

* Still working on the MetaSat paper

### LSF Updates

* Finished transmission endpoint, working on satellite one
  * How do you nest terms in MetaSat, and make sure that people use the same structure? MetaSat and JSON-LD are both non-hierarchical.
    * For example, some of the same terms may be used under different terms, so they may show up more than once in any file. So how, for example, how do you tell the difference between "downlink frequency" and "uplink frequency" when they both use the "frequency" term?
  * Might be able to force a structure on people who upload data (such as, have to put "frequency" nested under "downlink" or "uplink"), OR could make more specific concepts, such as "downlinkFrequency" and "uplinkFrequency"
    * This is something we've been going back and forth about for a while, might make this an issue on GitLab
  * Do other linked data vocabularies have enforced hierarchy? Not really
    * For example, schema.org has a structure but it's not strictly enforced, and some concepts are flexible (for example, two different databases may use different schema.org terms to stand in for the same thing)

### Wolbach Updates

* NASA proposal draft is ready
