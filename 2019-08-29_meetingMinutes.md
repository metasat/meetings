## 08 29, 2019 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Vasilis Tsiligiannis, Manthos Papamatthaiou, Fredy Damkalis
**Meeting purpose**: Updates from both teams

*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; ERD - Entity Relationship Diagram; SAA: Space Act Agreement; CSV: Spreadsheet file format; JSON: Metadata file format)*

#### Action Review  
* [Issue #7][licensing]: Try obtaining a license in Namibia/Zambia
    * Pierros is on vacation, will update next week
    * Wolbach wants to send emails by end of next week
* [Issue #10][shortlist]: Contact potential partner libraries
    * Contacted libraries in Brazil and South Dakota--waiting for issue #7 to resolve before contacting another
* [Issue #13][website-outline]: Review outline for website
* [Issue #16][schema-feedback]: Feedback on current schema using website dendrogram
    * Can do this using feedback form on website--gets emailed to Daniel, and he can make a GitLab issue
      * Drag fields to "orphans" node to "delete" during feedback
      * GitLab issues will be added to future schema project
* [Issue #18][Eastern-libraries]: Research backup countries for Eastern hemisphere libraries
    * Waiting until we hear back about Namibia (issue #7)
* [Issue #19][kit-test]: Test the LSTN kit for feasibility
    * Hardware has been assembled but not online yet; will get another update in the next week
* [Issue #21][stakeholders]: Draft list of stakeholders to contact
    * Daniel and Allie are drafting [a list of people to contact][stakeholders-sheet] for feedback--does LSF have anyone they want to add to the list?
    * Two pronged approach: Will invite a handful of people directly, and add a forum post to the website
    * Eventually, when we want broad feedback, will create a survey to send out
* All other actions were completed or are no longer relevant.

#### Action Items  
| Issue number | Summary | Assignee |
| --- | --- | --- |
| [22](https://gitlab.com/metasat/meetings/issues/22) | Email specific individuals who may want to give feedback on MetaSat schema | |
| [23](https://gitlab.com/metasat/meetings/issues/23) | Make a forum post soliciting feedback on schema | |
| [24](https://gitlab.com/metasat/meetings/issues/24) | Make a sharable version of SatNOGS ERD definitions list | AW |
| [25](https://gitlab.com/metasat/meetings/issues/25) | Schedule meeting with MIT including LSF | |
| [26](https://gitlab.com/metasat/meetings/issues/26) | Nate's signoff on use of Sloan branding for the website | |
| [27](https://gitlab.com/metasat/meetings/issues/27) | Update timeline for tasks | DB, DC |

#### Logistics  
* Wiki page has been added to GitLab--this can be used to share images and documents
    * This keeps documents from becoming buried in issues and makes them easily findable/shareable

#### LSTN Updates  
* Have reached out to potential partner libraries in Brazil and South Dakota
* Have heard back from Cambridge Public Library
    * May run a test-build with Cambridge Public Library with the LSTN kit
    * This way, can compile a set of instructions to send to other partner libraries
* Meeting with MIT to discuss possible avenues for developing curricula relating to LSTN
    * Will schedule so that both Wolbach and LSF can attend
* Daina and Katie meeting with Craig Burkhard about SAA--will have update next week
    * Still unclear if a separate SAA will be needed for LSF
    * Will we have regular meetings with NASA? Still unsure

#### MetaSat Updates  
* [Dendrogram of schema is on the website][website-schema]
    * Contact information seems redundant---may want to use [X.500 schema][X500] to help here
    * Clarification: Website schema is NOT a database
      * Databases try to reduce redundancy, but this is just a visualization
    * Work in progress to make the dendrogram look more legible
* [CSV][CSV-schema] and JSON files for the schema also exist, may be added to GitLab soon
    * Will be added to a new project
    * Katie's script transforms the CSV/JSON file into a dendrogram
      * The dendrogram is NOT an ERD
* Daina shared file describing [potential use cases][use-cases-narrative]--make sure that schema can be used for each

<!-- Links -->
[website-schema]: https://metasat.schema.space
[X500]: https://tools.ietf.org/html/rfc2256
[CSV-schema]: https://docs.google.com/spreadsheets/d/1H50LFo0cTwJFByDomOIG5gXSNRL5ybmq2RBZ9zHMCSU/edit#gid=1580995947
[use-cases-narrative]: https://docs.google.com/document/d/10xPXfmeLe1k6Feis5JQvqP-oeGSFuSBp-fnGJd3tVWw/edit#heading=h.11w9j2ymvizv
[stakeholders-sheet]: https://docs.google.com/spreadsheets/d/1890EH4VX9BCBssziGSwsGSYFQFIvm7urIDALQK7KKr4/edit#gid=0

[licensing]: https://gitlab.com/metasat/meetings/issues/7
[shortlist]: https://gitlab.com/metasat/meetings/issues/10
[website-outline]: https://gitlab.com/metasat/meetings/issues/13
[schema-feedback]: https://gitlab.com/metasat/meetings/issues/16
[Eastern-libraries]: https://gitlab.com/metasat/meetings/issues/18
[kit-test]: https://gitlab.com/metasat/meetings/issues/19
[stakeholders]: https://gitlab.com/metasat/meetings/issues/21
