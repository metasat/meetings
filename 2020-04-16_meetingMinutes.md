# 16 April 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update | PP |

**All other actions were completed or are no longer relevant.**

<!--
### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #41 |  | XX |
-->

## Updates

### LSTN Updates

* Handbook is coming along nicely
  * Working on moving to publishing software to make professional PDF files
* Contacted partner libraries&mdash;most are not open but working from home
  * We should be able to send our kits to most next week
* Is there an ETA for changing UI for choosing antenna type on SatNOGS website?
  * Hoping to finish today, should be tested and tomorrow or weekend will be in production
  * Can get screenshots from [dev instance](network-dev.satnogs.org)

### MetaSat Updates

* Finished [EPS draft](https://gitlab.com/metasat/metasat-schema/-/blob/master/Examples/ElectricalPowerSystems.jsonld)
* Finished AOCS terms&mdash;will be sharing spreadsheet soon
* Timeline for LSF's implementation of MetaSat?
  * Ask Pierros
* Met with NASA about [SPOON](https://spoonsite.com/) implementation

<!--
### LSF Updates

* x

### Wolbach Updates

* x
-->
