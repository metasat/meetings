# 17 June 2021 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Pierros Papadeas, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; UNOOSA - UN Office for Outer Space Affairs)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* [Translations](https://hosted.weblate.org/projects/library-space-technology-network/) going well; Romanian is done!
    * Librarian at the Biblioteca de Santiago has been working with a SatNOGS contributor about the Spanish-language translations, will probably meet soon to figure something out
* Still working on troubleshooting for Santiago, Chile ground station
* Pierros is having a meeting with a UNOOSA group, hoping to talk about LSTN there.
    * May talk about distributing LSTN kits and using the handbook and other resources
    * Group he is talking to is hoping to lower the budget even more, if possible; thinking around $200 each
        * This would be very stripped down, but SatNOGS will help them out to figure this out

<!--### MetaSat Updates 

* x

### LSF Updates

* x

### Wolbach Updates

* x
-->