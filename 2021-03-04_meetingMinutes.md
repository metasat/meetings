# 4 March 2021 MetaSat Meeting Minutes

**Time:** 15:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* [LSTN Handbook](https://hosted.weblate.org/projects/library-space-technology-network/) Weblate project is up and approved for libre hosting
  * Who can we work with to announce this? Through SatNOGS, Nikoletta (comms person); will get in contact with Wolbach
  * Should we write a how-to translate? 
    * Find what Weblate already has and supplement it (for example, what is a "string"?)
* Starting to get back in contact with our partner librarians

### MetaSat Updates 

* Daniel working on MINXSS JSON-LD file and the MetaSat paper
  * Rough due date: April 16

### LSF Updates

* Started to work on changes to import data using JSON-LD
  * If Wolbach can provide examples for testing, let us know
  * There is no pre-existing JSON-LD validator; what are your plans for this?
    * API endpoints have features for validating data; should be able to work fine in combination with `@context` section of JSON-LD

### Wolbach Updates

* Nico, Katie, and Daina met with StarNet libraries team
  * Want to talk with Pierros about this
  * NASA does not like sending money out of the US; Is there anyone on the SatNOGS team in the US who can help us with development?
