# 11 June 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Nico Carver, Daniel Chivvis, Allie Williams, Vasilis Tsiligiannis, Fredy Damkalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |
| #41 | Add MetaSat primer to schema repository | No update | AW |

<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #42 | --- | XX |
-->

## Updates

### LSTN Updates

* Finally got a go-ahead for FedEx, should hopefully be able to ship LSTN kits tomorrow!

### MetaSat Updates

* Working on paper, new version of sorting tool (finished part of space segment)
  * Looking into NASA thesaurus, might help with our schema design
  * Working on crosswalk - what else can we link to?
  * Thank you Fredy for feedback!
  * Probably will do a batch update of website next week
* Working on documentation
  * This will hopefully help make MetaSat easier to understand and implement
  * Will start working on incorporating into website soon

### LSF Updates

* Vasilis working on client and artifacts
  * New client update released recently and have been working through bugs
  * Will hopefully have something for artifacts next week
  * Update your stations now! Should help with USB/LimeSDR error
* Fredy working on exporting functionality of SatNOGS DB to MetaSat schema
  * When might we be able to see a prototype?
    * Hopefully by the end of the month
  * Right now working on exporting to JSON-LD in general, and then will port to MetaSat specifically once we have all of the details

<!--
### Wolbach Updates

* x
-->