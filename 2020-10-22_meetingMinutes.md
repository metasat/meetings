# 22 October 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Daniel Chivvis, Fredy Damkalis, Katie Frey, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

<!--
**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  

### LSTN Updates

* Chile has committed to put their station up soon
  * Their building is opening, but can't do a community build right now

### MetaSat Updates

* Working on MetaSat paper
  * Discovering potential contacts in the academic literature
  * Group at Rowan University - interested in spacecraft metadata
    * Master's thesis- IEEE standard for smart transducers (has its own file format), and used to bundle metadata for spacecraft interfaces
    * Daniel emailed advisor to talk to their group; they're interested in MetaSat
  * [ORCHIDS project](https://ml.jpl.nasa.gov/projects/orchids/orchids.html) - Jet Propulsion Lab group working on data side of mission payloads
    * Trying to organize in a specific file type for ease of access for all data throughout the mission pipeline
  * Right now, both interfaces and data are out of our scope, but could be complimented by these groups
* Working on documentation for the website 
  * Editing pages for clarity and to be up-to-date
  * Adding a page to clarify on URLs/URIs and how they work with linked data
* Working on JSON-LD generator
  * Current focus: Importing a structure document, and being able to add or change fields from there

### LSF Updates

* Working on last review for the Qubik mission; taking up a lot of time and energy right now
* Some updates on artifacts&mdash;backend changes to the DB so artifacts can be stored

<!--
### Wolbach Updates

* x
-->