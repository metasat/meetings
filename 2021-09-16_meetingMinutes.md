# 16 September 2021 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Nico Carver, Daniel Chivvis, Fredy Damkalis, Nikoletta Triantafyllopoulou, Allie Williams  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; LISA - Library and Information Services in Astronomy)*  
<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  

### LSTN Updates

* Nico working on LSTN papers

### MetaSat Updates  

* Wikidata mix and match tool - added MetaSat
* Update to Metasat database with new concepts, and possibly deprecating old concepts, coming soon

### LSF Updates

* Working on report for Sloan

<!--### Wolbach Updates

* x-->
