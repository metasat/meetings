# 24 September 2020 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Katie Frey, Pierros Papadeas, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |
<!--
**All other actions were completed or are no longer relevant.**

### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->

## Updates  

### LSTN Updates

* Cambridge Public Library: Decided that since there's no plan to open for in-person workshops soon, will put the ground station up this fall 
  * Will Nico build it? One of their librarians? We hope their librarians will, and give us feedback on the handbook
* Other libraries in statis; working on LSTN activities - [feedback form](https://forms.gle/ZsbYuRZpbdPGK81u8)
* Getting quotes for the cost of translating the handbooks soon

### MetaSat Updates

* Working on RDF/XML versions of MetaSat
  * It's sort of a legacy format at this point; we might use [Turtle](https://www.w3.org/TR/turtle/) instead
  * Might write in Turtle and put it in our GitLab, then use a [PURL](http://www.purlz.org/) to redirect to the URL for the README
  * There is a W3C standard for this kind of README that we will try to follow
  * After the RDF is written, whether in RDF/XML or Turtle, it can be converted using a software called Protege; can convert between Turtle, RDF/XML, OWL, maybe JSON-LD
* Crosswalked to a vocabulary of meteorology
* Looking through different Databases ([Celestrak](https://celestrak.com/), [Nanosats](https://www.nanosats.eu/), [Heavens Above](https://www.heavens-above.com/), [Michael Swartwout's CubeSat database](https://sites.google.com/a/slu.edu/swartwout/home/cubesat-database)...) to see how they describe their data to see if we can crosswalk fully to MetaSat
* How to distinguish mission name and spacecraft name? Might make new elements to distinguish them. Also instrument name.
* Soon: Working on documentation and cleaning up the [GitLab](https://gitlab.com/metasat/metasat-schema)
  * Moving forward, will use a versioning cycle so that it isn't just constantly changing
* Trying to learn more about JSON-LD algorithms and how we can use them.

### LSF Updates

* New developments were deployed - mission related information moved from Network to DB (orbital elements, TLEs)
  * Info is MetaSat friendly!
  * Next step: Enabling Satellite ID (unique thing to characterize each spacecraft) - defines satellite after launch and verification - does not exactly match with NORAD ID
    * What format?  How will it work with MetaSat? Think [randomly generated IDs](https://github.com/ai/nanoid) similar to UUID right now; is there anything more sensible?
    * Should mostly effect how you verify information
    * Probably just a fixed-length ID - ~12 letters/numbers
    * Through MetaSat, there shouldn't be any restrictions to what kind of ID you use/what it looks like
      * Might be included in future SatNOGS frames

<!--
### Wolbach Updates

* x
-->