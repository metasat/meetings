# 29 April 2021 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Nico Carver, Daniel Chivvis, Fredy Damkalis, Katie Frey, Becca Tibbitts, Nikoletta Triantafyllopoulou, Allie Williams   
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network)*

<!--
## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | ---------------- | ---------- | --- |
| #36 | Research radio reception laws in Moldova | No update, on hold | PP |

**All other actions were completed or are no longer relevant.**


### Logistics  

* x

## Action Items

| Issue number | Summary | Assignee |
| --- | ---------------- | --- |
| #43 | ---------------- | --- |
-->
## Updates  


### LSTN Updates

* Nikoletta has started the Weblate article and should be able to send out a link to a draft tomorrow
  * Will also share a social media strategy soon
  * Another plan: Who to contact if you have questions? Should there be a dedicated contact person?
    * Maybe could link the MetaSat Matrix channel


### MetaSat Updates 

* MetaSat JSON-LD generator demo
  * Right now, if you upload a "blank" JSON-LD file, it will let you fill it out as a form and then download a new file.
  * In the future, will be able to use forms that we create, or make a form from scratch using the MetaSat terms.
  * All of this can be done in a browser, currently on a [test server](https://test.schema.space/)
  * Next, want to work on improving the user interface.
  * Might want to share the code more openly; a public GitHub?
  * Will probably want to mint a release with a DOI before the MetaSat paper comes out.

### LSF Updates

* Nothing to share yet with the JSON-LD API; running into issues with formatting of data. Want to create a validation process.
  * Already framing data so that it will match with variable names and Django platform in use.
  * Also want validation of the structure, not just the fields

### Wolbach Updates

* Draft for NASA ROSES proposal due next Wednesday! Will share a more "final" draft soon.
