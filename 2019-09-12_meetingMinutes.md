# September 12, 2019 MetaSat Meeting Minutes

**Time:** 14:00 UTC  
**Attendees:** Daina Bouquin, Katie Frey, Pierros Papadeas, Nico Carver, Allie Williams, Daniel Chivvis, Vasilis Tsiligiannis, Fredy Damakalis  
**Meeting purpose**: Updates from both teams  
*(Acronyms: LSF - Libre Space Foundation; LSTN - Library Space Technology Network; ERD - Entity Relationship Diagram; SAA: Space Act Agreement; CSV: Spreadsheet file format; JSON: Metadata file format; CI - Continuous Integration)*

## Action Review

| Issue number | Summary | Discussion | Assignee |
| --- | --- | --- | --- |
| #7 | Attempt to obtain radio license in Namibia/Zambia | Not getting any responses, next step is maybe to call or fax the government; might drop this one | PP |
| #10 | Contact potential partner libraries | Updates in LSTN section, will be contacting additional libraries on Monday | NC |
| #16 | Review current schema using website dendrogram and JSON file | Ongoing, see MetaSat section for more details | FD |
| #19 | Test LSTN kit | Works now, [ground station 977](https://network.satnogs.org/stations/977/) in the SatNOGS network | PP |
| #21 | Draft list of stakeholders to contact | Ongoing | DC, AW |
| #22 | Email specific individuals who may want to give feedback on MetaSat schema | Will work on this this week | DC |
| #30 | Draft emails to LSF contacts; run by LSF | Will work on this week | DC |

*All other actions were completed or are no longer relevant.*

## Action Items

| Issue number | Summary | Assignee |
| --- | --- | --- |
| #31 | Confirm workflow for changes in schema and sharing changes with Libre Space | DB |
| #32 | Email about educational opportunities | DB |
| #33 | Draft LSTN document: What libraries can do with a ground station | NC |

## Updates

### LSTN Updates

* Have emailed some potential partner libraries with no/little response
  * Will give about 3 weeks for a response before moving on
  * May also try tweaking welcome email
* Eastern hemisphere
  * No luck getting radio license in Namibia/Zambia
  * Will be contacting library in [India](https://docs.google.com/document/d/1oyJW9FwCg_elBPc98aALWC5pvqB5-I_VizOnEScvUpY/edit) on Monday
  * Next choice would be Fiji
* Southern hemisphere
  * Have not heard back from Forest Library in Brazil (might be bad timing)
  * Will contact library in [Fortaleza, Brazil](https://docs.google.com/document/d/1dIWOsOfL-uSNs7pLHLctNmpdK3U7z9GN65BEr-Fyr0M/edit) next
* Rural US
  * Still waiting to hear back from Woksape Tipi
  * Will try a library in Montana next
* New kit setup works!
  * May experiment replacing some parts with low-cost pieces to see if they perform well
  * Pierros is documenting the process of this as he keeps tweaking the potential kit
  * Wolbach has not ordered kit pieces yet--waiting for stuff to get finalized

### MetaSat Updates

* More discussion on sharing schema tool JSON on GitLab
  * Discussion ongoing; will update over the next week.
* Lots of updates to MetaSat this week!
  * Updated [Google sheet](https://docs.google.com/spreadsheets/d/1NtFqS9KIKNw15nbbHqUgAeUPxqIjb2_iN5fkKpkSclo/edit#gid=704841001) and [schema tool](https://metasat.schema.space)
  * Incorporated Fredy's feedback and added some stuff
  * Questions about transmitter, transceiver, transponder; what's the best way to describe and separate these?
    * Will continue to discuss over email/Riot
  * Want to be able to share with NASA soon
  * May eventually expand to cover ALL satellites instead of just CubeSats
* Future to-do: What do we want feedback on?
  * Will want to draft a questionnaire
  * Want to contact many types of people: Satellites, radio, legislature, etc...all have different vocabulary, needs, etc.
  * Want to think of what's difficult to define or not clear between different communities

### Wolbach Updates

* LSTN educational curriculum
  * Cambridge Public Library conversation
    * Will put together instructions for a community build between Wolbach, LSF, and MIT
    * CPL will be the first test build using the instructions!
  * Nico will draft document on what is possible with a ground station
  * Daina will email LSF about additional educational discussions with MIT and NASA
